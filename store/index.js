export const actions = {
  // nuxtServerInit does not work in modules, only in root store.
  async nuxtServerInit({ commit }, { $content, error }) {
    try {
      // Get the content data for the menu.
      const menu = await $content('/', { deep: true })
        .where({ slug: { $ne: 'index' } })
        .only(['title', 'slug', 'dir', 'path', 'toc'])
        .sortBy('slug')
        .fetch()
      commit('menu/setMenu', menu, { root: true })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('error', e)
      error(e)
    }
  },
}
