export const state = () => ({
  menu: [],
})

export const mutations = {
  setMenu(state, items) {
    state.menu = items
  },
}

export const getters = {
  getMenu: (state) => {
    return state.menu
  },
}
