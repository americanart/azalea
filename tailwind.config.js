const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')
const sanSerifFamily = [
  'Inter',
  'ui-sans-serif',
  'system-ui',
  '-apple-system',
  'BlinkMacSystemFont',
  'Segoe UI',
  'Roboto',
  'Helvetica Neue',
  'Arial',
  'Noto Sans',
  'sans-serif',
  'Apple Color Emoji',
  'Segoe UI Emoji',
  'Segoe UI Symbol',
  'Noto Color Emoji',
]
module.exports = {
  purge: {
    content: [
      './assets/css/tailwind.css',
      './components/**/*.{vue,js}',
      './content/**/*.md',
      './layouts/**/*.vue',
      './pages/**/*.vue',
      './plugins/**/*.{js,ts}',
      './nuxt.config.{js,ts}',
    ],
    options: {
      safelist: [
        /-(leave|enter|appear)(|-(to|from|active))$/,
        /^(?!(|.*?:)cursor-move).+-move$/,
        /^router-link(|-exact)-active$/,
        /^[wh]-/,
        /^align-/,
        /^(top|bottom|left|right|inset|-top|-bottom|-left|-right|-inset)-/,
        /^(bg|text|border)-(primary|secondary|tertiary)-/,
        /^(xs|sm|md|lg|xl):[wh]-/,
        /^(xs|sm|md|lg|xl):justify-/,
        /^(m|mt|mb|mr|ml|mx|my|p|pt|pb|pr|pl|px|py)-/,
        /^(xs|sm|md|lg|xl):(m|mt|mb|mr|ml|mx|my|p|pt|pb|pr|pl|px|py)-/,
        /^(space-x|space-y)-(1|2|3|4|5|6|7|8|9|10)/,
        /^(xs|sm|md|lg|xl):(space-x|space-y)-(1|2|3|4|5|6|7|8|9|10)/,
        /^object-/,
        /^max-[wh]-/,
        /^(xs|sm|md|lg|xl):max-[wh]-/,
        /^whitespace-/,
        /^(grid-cols|grid-rows|grid-flow|auto-rows|gap)-/,
        /^(xs|sm|md|lg|xl):(grid-cols|grid-rows|gap)-/,
        /^items-/,
        /^transform$/,
        /^(aspect|opacity|ease|translate|-translate|scale|duration)-/,
        /^azalea-/,
        /^(xs|sm|md|lg|xl):azalea-/,
        /^(xs|sm|md|lg|xl):(visible|invisible|hidden)/,
        /^hover:(border|bg|text|shadow)-/,
      ],
      extractors: [
        {
          extractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
          extensions: ['vue', 'js', 'html'],
        },
      ],
    },
  },
  theme: {
    extend: {
      fontFamily: {
        heading: sanSerifFamily,
        body: sanSerifFamily,
      },
      boxShadow: {
        highlight: '0 10px 15px -3px rgba(55, 48, 163, 0.1), 0 4px 6px -2px rgba(55, 48, 163, 0.05)',
      },
      screens: {
        print: { raw: 'print' }, // @media print
        portrait: { raw: '(orientation: portrait)' }, // @media (orientation: portrait)
        ...defaultTheme.screens,
      },
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      primary: {
        DEFAULT: '#525252',
        50: '#C5C5C5',
        100: '#B8B8B8',
        200: '#9F9F9F',
        300: '#858585',
        400: '#6C6C6C',
        500: '#525252',
        600: '#393939',
        700: '#1F1F1F',
        800: '#050505',
        900: '#000000',
      },
      secondary: colors.blue,
      tertiary: colors.coolGray,
      success: '#10b981',
      warning: '#f59e0b',
      error: '#ef4444',
    },
  },
  variants: {
    extend: {
      brightness: ['hover', 'focus'],
    },
  },
  corePlugins: {
    divideStyle: false,
  },
  plugins: [require('@tailwindcss/aspect-ratio'), require('@tailwindcss/forms')],
}
