---
title: Azalea Design System
description: Design System and component library for the Smithsonian American Art Museum.
---

<h2 class="my-4">Overview</h2>

*Azalea* is a Design System and pattern library for the [Smithsonian American Art Museum](https://americanart.si.edu).
It serves as a resource that helps to define and implement a common visual language for our web applications. 


<h2 class="my-4">Purpose</h2>

To further the museum’s mission of collecting and celebrating American art by delivering the museum’s message to new audiences through modern digital channels. Applications developed and maintained are resources that support all the museum’s goals. They can speak to 21st-century audiences. Provide resources for educators across the country, and enable access to collections, and reflections on those objects, to audiences that may never visit the museum in person.")

A design system also:
- Minimizes customizations while meeting user and business needs
- Produces scalable and maintainable code
- Brings together user experience and visual savvy
- Sets an example, showing others the value and effectiveness of good design and engineering


