---
title: Color
---

Designed to be beautiful, expressive and accessible, Azalea's color tokens can be used to create recognizable
consistency in any SAAM project.

<h2 class="my-4">Core</h2>

<div class="min-w-0 flex-1 grid grid-cols-5 2xl:grid-cols-10 gap-x-4 gap-y-3 2xl:gap-x-2">
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-black"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">Black</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-white"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">White</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-success"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-full font-medium text-tertiary-900">Success</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-warning"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-full font-medium text-tertiary-900">Warning</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-error"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-full font-medium text-tertiary-900">Error</div>
      </div>
   </div>
</div>


<h2 class="my-4">Primary</h2>

<div class="min-w-0 flex-1 grid grid-cols-5 2xl:grid-cols-10 gap-x-4 gap-y-3 2xl:gap-x-2">
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-50"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">50</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-100"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">100</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-200"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">200</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-300"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">300</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-400"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">400</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-500"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">500</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-600"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">600</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-700"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">700</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-800"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">800</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-primary-900"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">900</div>
      </div>
   </div>
</div>

<h2 class="my-4">Secondary</h2>

<div class="min-w-0 flex-1 grid grid-cols-5 2xl:grid-cols-10 gap-x-4 gap-y-3 2xl:gap-x-2">
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-50"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">50</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-100"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">100</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-200"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">200</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-300"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">300</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-400"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">400</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-500"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">500</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-600"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">600</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-700"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">700</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-800"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">800</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-secondary-900"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">900</div>
      </div>
   </div>
</div>

<h2 class="my-4">Tertiary</h2>

<div class="min-w-0 flex-1 grid grid-cols-5 2xl:grid-cols-10 gap-x-4 gap-y-3 2xl:gap-x-2">
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-50"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">50</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-100"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">100</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-200"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">200</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-300"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">300</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-400"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">400</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-500"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">500</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-600"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">600</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-700"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">700</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-800"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">800</div>
      </div>
   </div>
   <div class="mb-4 md:mb-6 space-y-1.5">
      <div class="h-10 w-full rounded shadow-sm bg-tertiary-900"></div>
      <div class="px-0.5 md:flex md:justify-between md:space-x-4 2xl:space-x-2 2xl:block">
         <div class="w-6 font-medium text-tertiary-900">900</div>
      </div>
   </div>
</div>

<h2 class="my-4">States</h2>

1. Default
2. Hover
3. Press/active
4. Focus
5. Disabled
6. Visited
7. Error
