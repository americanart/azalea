---
title: Type
---

Azalea uses type tokens to manage typography. These tokens contain values, such as size, line-height, and letterspacing. Our type scale is refined in `rem` units, meaning `1rem` is always equal to the font-size defined in our `<html>`.

Proper text contrast is essential for our users with low vision. Our typography meets WCAG 2.0 contrast standards to the AA level, which is defined as:

1. A contrast ratio of 4.5:1 for normal-sized text
2. A contrast ratio of 3:1 for large-sized text


<h2 class="my-6">Headings</h2>

Headings can be displayed in any combination of these 5 font size and 7 font weight variations:

- size: <span class="text-sm text-tertiary-400">xsmall, small, medium, large, xlarge</span>
- weight: <span class="text-sm text-tertiary-400">thin, extralight, light, normal, semibold, bold, extrabold</span>

<h3 class="my-4">Default Headings</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <h1>Default heading level 1</h1>
  <h2>Default heading level 2</h2>
  <h3>Default heading level 3</h3>
  <h4>Default heading level 4</h4>
  <h5>Default heading level 1</h5>
  <h6>Default heading level 6</h6>
</div>

<h3 class="my-4">XLarge Headings</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-heading size="xlarge" weight="thin">XLarge thin heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="extralight">XLarge extralight heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="light">XLarge light heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="normal">XLarge normal heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="semibold">XLarge semibold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="bold">XLarge bold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="extrabold">XLarge extrabold heading</azalea-sprout-heading>
</div>
<h3 class="my-4">Large Headings</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-heading size="large" weight="thin">Large thin heading</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="extralight">Large extralight heading</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="light">Large light heading</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="normal">Large normal heading</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="semibold">Large semibold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="bold">Large bold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="extrabold">Large extrabold heading</azalea-sprout-heading>
</div>
<h3 class="my-4">Medium Headings</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-heading size="medium" weight="thin">Medium thin heading</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="extralight">Medium extralight heading</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="light">Medium light heading</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="normal">Medium normal heading</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="semibold">Medium semibold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="bold">Medium bold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="extrabold">Medium extrabold heading</azalea-sprout-heading>
</div>
<h3 class="my-4">Small Headings</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-heading size="small" weight="thin">Small thin heading</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="extralight">Small extralight heading</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="light">Small light heading</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="normal">Small normal heading</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="semibold">Small semibold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="bold">Small bold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="extrabold">Small extrabold heading</azalea-sprout-heading>
</div>
<h3 class="my-4">XSmall Headings</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-heading size="xsmall" weight="thin">XSmall thin heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="extralight">XSmall extralight heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="light">XSmall light heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="normal">XSmall normal heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="semibold">XSmall semibold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="bold">XSmall bold heading</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="extrabold">XSmall extrabold heading</azalea-sprout-heading>
</div>

<h2 class="my-4">Subhead</h2>
<mark>TODO</mark>



<h2 class="my-4">Body Text</h2>

Body text can also be displayed in any combination of these 5 font size and 7 font weight variations:

- size: <span class="text-sm text-tertiary-400">xsmall, small, medium, large, xlarge</span>
- weight: <span class="text-sm text-tertiary-400">thin, extralight, light, normal, semibold, bold, extrabold</span>

<h3 class="my-4">Default Body Text</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-body-text>The Smithsonian American Art Museum is dedicated to collecting, understanding, and enjoying American art. The Museum celebrates the extraordinary creativity of artists whose works reflect the American experience and global connections.</azalea-sprout-body-text>
</div>

<h3 class="my-4">XLarge Body Text</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-body-text size="xlarge" weight="thin">XLarge thin body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xlarge" weight="extralight">XLarge extralight body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xlarge" weight="light">XLarge light body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xlarge" weight="normal">XLarge normal body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xlarge" weight="semibold">XLarge semibold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xlarge" weight="bold">XLarge bold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xlarge" weight="extrabold">XLarge extrabold body text</azalea-sprout-body-text>
</div>
<h3 class="my-4">Large Body Text</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-body-text size="large" weight="thin">Large thin body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="large" weight="extralight">Large extralight body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="large" weight="light">Large light body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="large" weight="normal">Large normal body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="large" weight="semibold">Large semibold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="large" weight="bold">Large bold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="large" weight="extrabold">Large extrabold body text</azalea-sprout-body-text>
</div>
<h3 class="my-4">Medium Body Text</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-body-text size="medium" weight="thin">Medium thin body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="medium" weight="extralight">Medium extralight body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="medium" weight="light">Medium light body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="medium" weight="normal">Medium normal body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="medium" weight="semibold">Medium semibold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="medium" weight="bold">Medium bold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="medium" weight="extrabold">Medium extrabold body text</azalea-sprout-body-text>
</div>
<h3 class="my-4">Small Body Text</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-body-text size="small" weight="thin">Small thin body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="small" weight="extralight">Small extralight body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="small" weight="light">Small light body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="small" weight="normal">Small normal body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="small" weight="semibold">Small semibold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="small" weight="bold">Small bold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="small" weight="extrabold">Small extrabold body text</azalea-sprout-body-text>
</div>
<h3 class="my-4">XSmall Body Text</h3>
<div class="resize-x p-6 border-1 border-tertiary-900">
  <azalea-sprout-body-text size="xsmall" weight="thin">XSmall thin body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xsmall" weight="extralight">XSmall extralight body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xsmall" weight="light">XSmall light body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xsmall" weight="normal">XSmall normal body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xsmall" weight="semibold">XSmall semibold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xsmall" weight="bold">XSmall bold body text</azalea-sprout-body-text>
  <azalea-sprout-body-text size="xsmall" weight="extrabold">XSmall extrabold body text</azalea-sprout-body-text>
</div>



<h2 class="my-4">Description Text</h2>

There are also several common base styles for text used to describe a media object or other content.

<h3 class="my-4">Caption</h3>

<azalea-sprout-description-text type="caption"> Thomas Moran, Cliffs of the Upper Colorado River, Wyoming Territory, 1882, oil on canvas, Smithsonian American Art Museum, Bequest of Henry Ward Ranger through the National Academy of Design, 1936.12.4 </azalea-sprout-description-text>

<h3 class="my-4">Credit</h3>

<azalea-sprout-description-text type="credit"> Photo by Marcus O'Leary </azalea-sprout-description-text>

<h3 class="my-4">Lead</h3>

<azalea-sprout-description-text type="lead"> July 20 would have been Nam June Paik's 80th birthday. To celebrate last year we enjoyed a cake inspired by Paik's work Electronic Superhighway: Continental U.S., Alaska, Hawaii, and this year I have heard a rumor that there will be <em>cupcakes</em>. </azalea-sprout-description-text>

<h3 class="my-4">Quote</h3>

<azalea-sprout-description-text type="quote"> I think making music is just what you're supposed to do because you just never know whose life you're saving at that moment or whose experience you're representing, whether you're aware of it or not.
. </azalea-sprout-description-text>

<script>
    import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText";
    export default {
        components: {AzaleaSproutBodyText}
    }
</script>
<script>
    import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText";
    export default {
        components: {AzaleaSproutBodyText}
    }
</script>
<script>
import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText"; export default { components: {
AzaleaSproutBodyText } }
</script>
<script>
import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText"; export default { components: {
AzaleaSproutBodyText } }
</script>
<script>
import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText"; export default { components: {
AzaleaSproutBodyText } }
</script>
<script>
import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText"; export default { components: {
AzaleaSproutBodyText } }
</script>
<script>
import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText"; export default { components: {
AzaleaSproutBodyText } }
</script>
<script>
import AzaleaSproutBodyText from "@/components/azalea/sprout/BodyText"; export default { components: {
AzaleaSproutBodyText } }
</script>
