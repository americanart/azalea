---
title: Dividers
description: Content dividers.
---

<demo-preview title="Small Divider" :class="`mb-8`">
  <div class="p-16 bg-white">
    <div class="relative py-2 md:py-4 max-w-screen-2xl mx-auto">
      <div class="absolute inset-0 flex items-center" aria-hidden="true">
        <div class="w-full border-tertiary-300 border-t"></div>
      </div>
    </div>
  </div>
</demo-preview>

<demo-preview title="Medium Divider" :class="`mb-8`">
  <div class="p-16 bg-white">
    <div class="relative py-2 md:py-4 max-w-screen-2xl mx-auto">
      <div class="absolute inset-0 flex items-center" aria-hidden="true">
        <div class="w-full border-tertiary-300 border-t-2"></div>
      </div>
    </div>
  </div>
</demo-preview>

<demo-preview title="Large Divider" :class="`mb-8`">
  <div class="p-16 bg-white">
    <div class="relative py-2 md:py-4 max-w-screen-2xl mx-auto">
      <div class="absolute inset-0 flex items-center" aria-hidden="true">
        <div class="w-full border-tertiary-300 border-t-4"></div>
      </div>
    </div>
  </div>
</demo-preview>

<demo-preview title="Divider with label" :class="`mb-8`">
  <div class="p-16 bg-white">
    <div class="relative py-2 md:py-4 max-w-screen-2xl mx-auto">
      <div class="absolute inset-0 flex items-center" aria-hidden="true">
        <div class="w-full border-tertiary-300 border-t"></div>
      </div>
      <div class="relative flex justify-center">
          <span class="px-2 bg-white text-sm text-tertiary-500">
            Read more
          </span>
      </div>
    </div>
  </div>
</demo-preview>
