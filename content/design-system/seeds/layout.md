---
title: Layout
---

<h2 class="my-4">Grid spacing</h2>

The design system uses the following spacing elements:

<label>Horizontal padding</label>
  
  | Breakpoint | rem    | px   | class   |
  |------------|--------|------|---------|
  | sm         | 1rem   | 16px | sm:px-4 |
  | md         | 1rem   | 16px | md:px-4 |
  | lg         | 1.5rem | 24px | lg:px-6 |
  | xl         | 1.5rem | 24px | xl:px-6 |
  
<label>Column and Row spacing (Gap)</label>

| Breakpoint | rem     | px  | class    |
|------------|---------|-----|----------|
| sm         | 0.25rem | 4px | sm:gap-1 |
| md         | 0.25rem | 4px | md:gap-1 |
| lg         | 0.5rem  | 8px | lg:gap-2 |
| xl         | 0.5rem  | 8px | xl:gap-2 |

## Grid layouts

<div class="p-4 bg-tertiary-50 rounded-sm overflow-x-auto">

  <azalea-sprout-grid class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
    <div class="shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
  </azalea-sprout-grid>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-3 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">3</div>
    <div class="col-span-3 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">3</div>
    <div class="col-span-3 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">3</div>
    <div class="col-span-3 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">3</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-4 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">4</div>
    <div class="col-span-4 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">4</div>
    <div class="col-span-4 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">4</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-5 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">5</div>
    <div class="col-span-5 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">5</div>
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-6 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">6</div>
    <div class="col-span-6 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">6</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-7 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">7</div>
    <div class="col-span-5 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">5</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-8 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">8</div>
    <div class="col-span-4 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">4</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-9 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">9</div>
    <div class="col-span-3 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">3</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-10 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">10</div>
    <div class="col-span-2 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">2</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-11 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">11</div>
    <div class="col-span-1 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">1</div>
  </div>

  <div class="grid grid-flow-col auto-cols-auto px-4 lg:px-6 py-1 gap-1 lg:gap-2">
    <div class="col-span-12 shadow-sm w-full h-16 p-6 font-medium bg-white rounded-md flex items-center justify-center">12</div>
  </div>

</div>

## Layout patterns

Common patterns are applied using layout components to ensure consistency.

- <a href="/design-system/components/sprout/flexible-box">FlexibleBox</a>
- <a href="/design-system/components/sprout/grid">Grid</a>
- <a href="/design-system/components/sprout/stacked-box">StackedBox</a>
