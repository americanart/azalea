---
title: Units
---

Our spacing system follows an 8pt grid, meaning space is defined by divisions of 8: 8px/16px/24px/ and so on for padding and margin between elements. In some cases, we will use 4px if we need to constrain proportions for a tight look.

<h2 class="my-4">Spacing</h2>

<div class="flex w-full">
  <span class="m-1 w-2 h-2 flex items-center justify-center bg-tertiary-50 border border-tertiary-200 rounded-sm">
    <span class="m-1 p-1 text-xs">1</span>
  </span>
  <span class="m-2 w-4 h-4 flex items-center justify-center bg-tertiary-50 border border-tertiary-200 rounded-sm">
    <span class="m-2 p-2 text-xs">2</span>
  </span>
  <span class="m-4 w-8 h-8 flex items-center justify-center bg-tertiary-50 border border-tertiary-200 rounded-sm">
    <span class="m-4 p-4 text-xs">4</span>
  </span>
  <span class="m-8 w-16 h-16 flex items-center justify-center bg-tertiary-50 border border-tertiary-200 rounded-sm">
    <span class="m-8 p-8 text-xs">8</span>
  </span>
  <span class="m-16 w-32 h-32 flex items-center justify-center bg-tertiary-50 border border-tertiary-200 rounded-sm">
    <span class="m-16 p-16 text-xs">16</span>
  </span>
</div>

<h2 class="my-4">Breakpoints</h2>

<div class="w-full overflow-y-auto">
  <div class="p-4 bg-tertiary-50 border border-tertiary-200 rounded-sm mb-2" style="width: 375px;">
    <span>640 (mobile)</span>
  </div>
  <div class="p-4 bg-tertiary-50 border border-tertiary-200 rounded-sm mb-2" style="width: 768px;">
    <span>768 (tablet)</span>
  </div>
  <div class="p-4 bg-tertiary-50 border border-tertiary-200 rounded-sm mb-2" style="width: 1024px;">
    <span>1024 (tablet and desktop)</span>
  </div>
  <div class="p-4 bg-tertiary-50 border border-tertiary-200 rounded-sm mb-2" style="width: 1440px;">
    <span>1280 (desktop)</span>
  </div>
  <div class="p-4 bg-tertiary-50 border border-tertiary-200 rounded-sm mb-2" style="width: 1920px;">
    <span>1536 (desktop)</span>
  </div>
</div>
