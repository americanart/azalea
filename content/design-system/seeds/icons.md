---
title: Icons
---

Our icons meet WCAG 2.0 contrast standards to the AA level, which is defined as:

1. A contrast ratio of 3.0:1 for icons
2. All button and link icons can be activated with ease


<h2 class="my-4">Brand</h2>

1. Accessibility
2. Dining
3. ~~Education~~
4. ~~Shop~~
5. ~~Support~~ ?
6. Stay connected

<div class="grid grid-cols-1 sm:grid-cols-4 md:grid-cols-6 lg:grid-cols-12 gap-2">

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="academic-cap" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">academic-cap</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="facebook" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">facebook</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="github" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">github</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="instagram" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">instagram</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="shopping-bag" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">shopping-bag</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="support" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">support</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="twitter" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">twitter</span>
  </div>

</div>

<h2 class="my-4">Standard</h2>

<div class="grid grid-cols-1 sm:grid-cols-4 md:grid-cols-6 lg:grid-cols-12 gap-2">

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="arrow-sm-down" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">arrow-sm-down</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="arrow-sm-left" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">arrow-sm-left</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="arrow-sm-right" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">arrow-sm-right</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="arrow-sm-up" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">arrow-sm-up</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="arrows-expand" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">arrows-expand</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="calendar" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">calendar</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="chevron-down" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">chevron-down</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="chevron-left" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">chevron-left</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="chevron-right" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">chevron-right</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="chevron-up" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">chevron-up</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="clipboard-copy" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">clipboard-copy</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="cog" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">cog</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="document-text" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">document-text</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="dots-vertical" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">dots-vertical</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="exclamation-circle" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">exclamation-circle</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="eye" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">eye</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="eye-off" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">eye-off</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="fast-forward" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">fast-forward</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="heart" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">heart</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="information-circle" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">information-circle</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="library" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">library</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="light-bulb" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">light-bulb</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="location-marker" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">location-marker</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="mail" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">mail</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="map" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">map</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="menu" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">menu</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="minus" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">minus</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="minus-circle" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">minus-circle</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="moon" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">moon</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="pause" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">pause</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="phone" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">phone</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="photograph" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">photograph</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="play" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">play</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="plus" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">plus</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="plus-circle" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">plus-circle</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="puzzle" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">puzzle</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="qrcode" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">qrcode</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="question-mark-circle" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">question-mark-circle</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="rewind" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">rewind</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="search" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">search</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="selector" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">selector</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="share" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">share</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="stop" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">stop</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="sun" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">sun</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="template" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">template</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="ticket" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">ticket</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="user" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">user</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="video-camera" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">video-camera</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="view-grid" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">view-grid</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="view-list" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">view-list</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="volume-off" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">volume-off</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="volume-up" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">volume-up</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="x" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">x</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="zoom-in" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">zoom-in</span>
  </div>

  <div class="flex flex-col p-6 justify-center rounded-md bg-tertiary-100">
    <div>
      <azalea-sprout-icon icon="zoom-out" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
    </div>
    <span class="text-sm text-center text-tertiary-700 break-normal">zoom-out</span>
  </div>

</div>
