---
title: Forms
---

<h2 class="my-4">Inputs</h2>
<azalea-sprout-container as="section" :class="`bg-white border border-tertiary-300 rounded-sm`">
  <h3 class="my-4">Text input with label</h3>
  <azalea-sprout-form-input label="First name" name="firstname"></azalea-sprout-form-input>
  <h3 class="my-4">Text input with hidden label and placeholder</h3>
  <azalea-sprout-form-input label="First name" name="firstname" :label-visible="false" placeholder="Jane"></azalea-sprout-form-input>
  <h3 class="my-4">Text input with help text</h3>
  <azalea-sprout-form-input label="Middle name" name="middle-name" help="Enter your middle name or leave blank for none."></azalea-sprout-form-input>
  <h3 class="my-4">Text input with hint text</h3>
  <azalea-sprout-form-input label="First name" name="firstname" hint="* required"></azalea-sprout-form-input>
  <h3 class="my-4">Text input with validation error</h3>
  <azalea-sprout-form-input label="First name" name="firstname" :error="true" error-message="This is a required value."></azalea-sprout-form-input>
  <h3 class="my-4">Text input with a leading icon</h3>
  <azalea-sprout-form-input label="Email" name="email" leading-icon="mail"></azalea-sprout-form-input>
</azalea-sprout-container>

<h2 class="my-4">Textarea</h2>
<azalea-sprout-container as="section" :class="`bg-white border border-tertiary-300 rounded-sm`">
  <h3 class="my-4">Textarea with help and hint text</h3>
  <azalea-sprout-text-area name="message" label="Message" help="Write a short message." hint="Optional" :resize="true"></azalea-sprout-text-area>
  <h3 class="my-4">Textarea with validation error</h3>
  <azalea-sprout-text-area name="message" label="Message" help="Write a short message." hint="Required" :error="true" error-message="This field is required"></azalea-sprout-text-area>
</azalea-sprout-container>

<h2 class="my-4">Checkboxes</h2>
<azalea-sprout-container as="section" :class="`bg-white border border-tertiary-300 rounded-sm`">
  <h3 class="my-4">Checkbox list</h3>
  <azalea-seedling-checkboxes legend="Interests" name="interests" :items="[
    { label: 'Museum E-news', description: 'The latest new and updates about american art.' },
    { label: 'Family Programs', description: 'Information about family friendly events at the museum.' },
    { label: 'All', description: 'I\'m interested in all topics.', checked: true },
  ]"></azalea-seedling-checkboxes>
  <h3 class="my-4">Checkbox list with validation error</h3>
  <azalea-seedling-checkboxes legend="Interests" name="interests" :error="true" error-message="Select at least one item from the list." :items="[
    { label: 'Museum E-news', description: 'The latest new and updates about american art.' },
    { label: 'Family Programs', description: 'Information about family friendly events at the museum.' },
    { label: 'All', description: 'I\'m interested in all topics.' },
  ]"></azalea-seedling-checkboxes>
</azalea-sprout-container>

<h2 class="my-4">Switch</h2>
<azalea-sprout-container as="section" :class="`bg-white border border-tertiary-300 rounded-sm`">
  <azalea-sprout-switch :default-enabled="true"></azalea-sprout-switch>
</azalea-sprout-container>

<h2 class="my-4">Range</h2>
<azalea-sprout-container as="section" :class="`bg-white border border-tertiary-300 rounded-sm`">
  <input class="azalea-range" type="range" min="5" max="200" step="5" value="5" />
</azalea-sprout-container>
