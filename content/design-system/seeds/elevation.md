---
title: Elevation
description: 
---

The elevation effect is made using shadows to help lift an element into focus as it might in the physical world. 

<h2 class="my-4">Core</h2>

<div class="grid grid-cols-2 gap-6 sm:grid-cols-3 md:grid-cols-4 p-4 bg-tertiary-50 rounded-sm">
  <div class="shadow-sm w-full h-32 md:h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">.shadow-sm</div>
  <div class="shadow w-full h-32 md:h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">.shadow</div>
  <div class="shadow-md w-full h-32 md:h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">.shadow-md</div>
  <div class="shadow-lg w-full h-32 md:h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">.shadow-lg</div>
  <div class="shadow-xl w-full h-32 md:h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">.shadow-xl</div>
  <div class="shadow-2xl w-full h-32 md:h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">.shadow-2xl</div>
</div>

<h2 class="my-4">Inner Shadow</h2>

<div class="p-4 bg-tertiary-50 rounded-sm">
  <div class="shadow-inner w-32 md:w-64 h-32 md:h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">.shadow-sm</div>
</div>
