---
title: Base styles
descriptions: Azalea base styles for common HTML elements
---

## Examples

### Small Paragraph

<div class="azalea-text-sm"> 
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>
</div>

### Medium/Regular Paragraph

<div class="azalea-text-md">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.
</p>
</div>

### Large Paragraph

<div class="azalea-text-lg">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.
</p>
</div>

<hr class="my-4"/>

### Section Headings &lt;h1>-&lt;h6>

<h1>Heading level 1</h1>
<h2>Heading level 2 - Primary</h2>
<h2 class="font-light">Heading level 2 - Subhead for Blogs</h2>
<h3>Heading level 3</h3>
<h4>Heading level 4</h4>
<h5>Heading level 5</h5>
<h6>Heading level 6</h6>

<hr class="my-4"/>

### Anchor &lt;a>

<a href="#">This is a link</a>

<hr class="my-4"/>

### Emphasis &lt;em>

<em>This is emphasized</em>

<hr class="my-4"/>

### Strong &lt;strong>

<strong>This is strong</em>

<hr class="my-4"/>

### Idiomatic Text &lt;i>

<i>This is idiomatic text</i>

<hr class="my-4"/>

### Mark Text &lt;mark>

<mark>This is marked/highlighted text</mark>

<hr class="my-4"/>

### Blockquote &lt;blockquote>

<blockquote>This is a blockquote</blockquote>

<hr class="my-4"/>

### Figure &lt;figcaption>

<figcaption> this is a caption </figcaption>

### Preformatted Text &lt;pre>

<pre>This is preformatted text</pre>

<hr class="my-4"/>

### Label &lt;label>

<label>Artwork is on view?
<input type="checkbox" name="on-view">
</label>

<hr class="my-4"/>

### Lists &lt;ul>&lt;ol>&lt;dl>

#### Unordered List

<ul>
  <li>Unordered list item one</li>
  <li>Unordered list item two</li>
  <li>Unordered list item three</li>
</ul>

#### Ordered List

<ol>
  <li>Ordered list item one</li>
  <li>Ordered list item two</li>
  <li>Ordered list item three</li>
</ol>

#### Description List

<dl>
    <dt>Title</dt>
    <dd>Electronic Superhighway: Continental U.S., Alaska, Hawaii</dd>
    <dt>Artist</dt>
    <dd>Nam June Paik</dd>
    <dt>Object Number</dt>
    <dd>2002.23</dd>
</dl>

<hr class="my-4"/>

<hr class="my-4"/>

### Table Caption &lt;caption>

<caption>This is a table caption, which is inserted immediately after the table tag </caption>

### Table

<table>
<caption>This is a table caption, which is inserted immediately after the table tag </caption>
    <thead>
        <tr>
            <th colspan="2">The table header</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Col one, row one</td>
            <td>Col two, row one</td>
        </tr>
        <tr>
            <td>Col one, row two</td>
            <td>Col one, row two</td>
        </tr>
    </tbody>
</table>

## Azalea reader

<div class="azalea-reader">
<h1>Heading level 1</h1>
<h2>Heading level 2 - Primary</h2>
<h3>Heading level 3</h3>
<h4>Heading level 4</h4>
<h5>Heading level 5</h5>
<h6>Heading level 6</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>
</div>
