---
title: Search Suggestion
description: A typehead text suggestion 
---

## Examples

<demo-preview title="With Icon" :class="`mb-8`">
  <azalea-seedling-search-suggestion icon-name="clock" text="Nam June Paik"></azalea-seedling-search-suggestion>
</demo-preview>

<demo-preview title="With link" :class="`mb-8`">
  <azalea-seedling-search-suggestion icon-name="user" text="Nam June Paik" :link="{ href: '#' }" link-text="View"></azalea-seedling-search-suggestion>
</demo-preview>
