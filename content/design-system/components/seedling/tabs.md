---
title: Tabs
description: Tabbed naviation element
---

## Examples

<demo-preview title="Events" :class="`mb-8`">
  <azalea-seedling-tabs :tabs="[ 
    { name: 'All', current: false, count: 1187 },
    { name: 'Artworks', current: false, count: 127 },
    { name: 'Artists', current: true, count: 15 },
    { name: 'Exhibitions', count: '2' },
  ]">
  </azalea-seedling-tabs>
</demo-preview>

<demo-preview title="Links" :class="`mb-8`">
  <azalea-seedling-tabs :tabs="[ 
    { name: 'Tabs Demo', current: true, href: '/design-system/components/seedling/tabs#' },
    { name: 'Events', href: '/design-system/components/seedling/tabs#' },
    { name: 'Tours', href: '/design-system/components/seedling/tabs#' },
    { name: 'Exhibitions', href: '/design-system/components/seedling/tabs#' },
  ]">
  </azalea-seedling-tabs>
</demo-preview>
