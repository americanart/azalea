---
title: Banner
description: A system wide notification
---

## Examples

<demo-preview title="Default Banner" :class="`mb-8`">
  <azalea-seedling-banner link="#" message="All public programs are online only, on-site public tours and events are currently suspended." brief="The current status of public tours has changed."></azalea-seedling-banner>
</demo-preview>

<demo-preview title="Secondary Banner" :class="`mb-8`">
  <azalea-seedling-banner variant="secondary" link="#" link-text="View event calendar" message="All public programs are online only, on-site public tours and events are currently suspended." brief="The current status of public tours has changed."></azalea-seedling-banner>
</demo-preview>

## Props

- variant
- dismissible <span class="text-sm text-tertiary-400">default: true</span>
- message
- brief
- link-text
- link

