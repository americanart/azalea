---
title: NavList
description: A navigation element
---

## Variants

- vertical <span class="text-sm text-tertiary-400">default</span>
- horizontal

## Examples

<demo-preview title="Vertical" :class="`mb-8`">
  <azalea-seedling-nav-list label="Vertical menu" :items="[
      { title: 'Art + Artworks', path: '#' },
      { title: 'Events + Exhibitions', path: '#'},
      { title: 'Plan your visit', path: '#' },
      { title: 'About the museum', path: '#' },
  ]"></azalea-seedling-nav-list>
</demo-preview>

<demo-preview title="Horizontal" :class="`mb-8`">
  <azalea-seedling-nav-list label="Horizontal menu" variant="horizontal" :items="[
      { title: 'Art + Artworks', path: '#' },
      { title: 'Events + Exhibitions', path: '#'},
      { title: 'Plan your visit', path: '#' },
      { title: 'About the museum', path: '#' },
  ]"></azalea-seedling-nav-list>
</demo-preview>
