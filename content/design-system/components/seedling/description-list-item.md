---
title: Description List Item
description: A single item for a Description List
---

Used in tombstones, accordions, and other elements that display a list of terms and descriptions or definitions for those terms.

## Examples

<demo-preview :class="`mb-8`">
  <dl>
    <azalea-seedling-description-list-item term="Title">
      <p>Electronic Superhighway: Continental U.S., Alaska, Hawaii </p>
    </azalea-seedling-description-list-item>
    <azalea-seedling-description-list-item term="Artist">
      <p>Nam June Paik</p>
    </azalea-seedling-description-list-item>
    <azalea-seedling-description-list-item term="Dimensions">
      <p>approx. 15 x 40 x 4 ft.</p>
    </azalea-seedling-description-list-item>
    <azalea-seedling-description-list-item term="Object Number">
      <p>2002.23</p>
    </azalea-seedling-description-list-item>
  </dl>
</demo-preview>
