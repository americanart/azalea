---
title: Alert
description: An inline or temporary notification
---

## Variants 

- primary
- secondary
- tertiary (default)

## Examples

<demo-preview title="Default Alert" :class="`mb-8`">
  <azalea-seedling-alert :dismissible="false" message="You've successfully complete task one!"></azalea-seedling-alert>
</demo-preview>

<demo-preview title="Dismissible Primary Alert" :class="`mb-8`">
  <azalea-seedling-alert variant="primary" message="You've successfully complete task one!"></azalea-seedling-alert>
</demo-preview>

<demo-preview title="Secondary Alert" :class="`mb-8`">
  <azalea-seedling-alert variant="secondary" message="You've successfully complete task one!"></azalea-seedling-alert>
</demo-preview>

## Props

- variant
- dismissible
- message

