---
title: Breadcrumb
description: A navigation element displaying the path of a page relative to a parent page.
---

## Examples

<demo-preview :class="`mb-8`">
    <azalea-seedling-breadcrumb :items="[
        { title: 'Home', link: { href: '#' } },
        { title: 'Art + Artworks', link: { href: '#' } },
        { title: 'On View' }
    ]"></azalea-seedling-breadcrumb>
</demo-preview>

## Props

- items <span class="text-sm text-tertiary-400">Array of nav items, includes `title` and `link` an Object with all link attributes</span>
- external  <span class="text-sm text-tertiary-400">default: false</span>
