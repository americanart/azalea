---
title: Dropdown
description: A container element used to display a menu or other content in a popover.
---

  <div class="flex items-center justify-center p-8">
    <azalea-seedling-dropdown label="Select" class="mt-6">
      <azalea-seedling-nav-list label="Vertical menu" :items="[
          { title: 'Art + Artworks', path: '#' },
          { title: 'Events + Exhibitions', path: '#'},
          { title: 'Plan your visit', path: '#' },
          { title: 'About the museum', path: '#' },
      ]"></azalea-seedling-nav-list>
    </azalea-seedling-dropdown>
  </div>

