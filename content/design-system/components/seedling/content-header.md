---
title: Content Header
description: A header for a blocks, content lists, or articles.
---

## Examples

<demo-preview title="Titled Section with dividers and CTA" :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Articles
        </h4>
        <div class="mt-3 flex sm:mt-0 sm:ml-4">
          <a href="#">See All</a>
        </div>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
</div>
</demo-preview>

<demo-preview title="Titled Section with dividers" :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Articles
        </h4>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
</div>
</demo-preview>

<demo-preview title="Titled Section with one divider and CTA" :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Articles
        </h4>
        <div class="mt-3 flex sm:mt-0 sm:ml-4">
          <a href="#">See All</a>
        </div>
      </div>
    </div>
  </div>
</div>
</demo-preview>

<demo-preview title="Titled Section with one divider" :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Articles
        </h4>
      </div>
    </div>
  </div>
</div>
</demo-preview>

<demo-preview title="Prominent Heading with link action" :class="`mb-8`">
<header class="azalea-block-header py-1 md:py-1 lg:py-4 border-t border-black">
  <div class="sm:flex sm:items-center sm:justify-between">
    <h3>
      K-12 Trainings
    </h3>
    <div class="mt-3 flex sm:mt-0 sm:ml-4">
      <a href="/design-system/components/seedling/content-header/#" target="" class="" title="">See all</a>
    </div>
  </div>
  <div class="azalea-text-lg">
    Discover how to integrate American art into your classroom.
  </div>
</header>
</demo-preview>

<demo-preview title="With link action" :class="`mb-8`">
<azalea-seedling-content-header heading-text="K-12 Trainings" description-text="Discover how to integrate American art into your classroom.">
<template #action>
<azalea-sprout-link href="#">See all</azalea-sprout-link>
</template>
</azalea-seedling-content-header>
</demo-preview>

<demo-preview title="With button action" :class="`mb-8`">
    <azalea-seedling-content-header heading-text="K-12 Trainings" description-text="Discover how to integrate American art into your classroom.">
      <template #action>
        <azalea-sprout-push-button href="#" variant="primary">Browse resources</azalea-sprout-push-button>
      </template>
    </azalea-seedling-content-header>
</demo-preview>

## Props

- heading-text
- description-text

## Slots

- action <span class="text-sm text-tertiary-400">Area for link or other action</span>
