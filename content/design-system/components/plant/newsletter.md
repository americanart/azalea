---
title: Newsletter
description: Pattern for newsletter signup
---

<demo-preview :class="`mb-8`">
<div class="container mx-auto">
<div class="bg-white">
  <div class="max-w-7xl mx-auto py-24 px-4 sm:px-6 lg:py-32 lg:px-8 lg:flex lg:items-center">
    <div class="lg:w-0 lg:flex-1">
      <h2>
        Sign up for our newsletter
      </h2>
      <p>
        Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui Lorem cupidatat commodo. Elit sunt amet fugiat veniam occaecat fugiat.
      </p>
    </div>
    <div class="mt-8 lg:mt-0 lg:ml-8">
      <form class="sm:flex">
        <label for="email-address" class="sr-only">Email address</label>
        <input id="email-address" name="email-address" type="email" autocomplete="email" required class="w-full px-5 py-3 border border-gray-300 shadow-sm placeholder-gray-400 focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs rounded-md" placeholder="Enter your email">
        <div class="mt-3 rounded-sm shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
         <azalea-sprout-push-button variant="default">Default</azalea-sprout-push-button>
        </div>
      </form>
      <p >
        We care about the protection of your data. Read our
        <a href="#" class="font-medium underline">
          Privacy Policy.
        </a>
      </p>
    </div>
  </div>
</div>
</div>
</demo-preview>
