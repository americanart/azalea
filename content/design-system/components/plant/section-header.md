---
title: Section Header
description: A page or section header with Heading, Body Text, and Image
---

## Variants

- default
- full

## Usage

- TBD


## Examples

<demo-preview title="Default" :class="`mb-8`">
  <azalea-plant-section-header 
    heading-text="How Do I Research My Art?" 
    body-text="Want to learn more about the painting you found while clearing out the attic? What about the drawing that has been hanging in grandma’s hallway since you were a kid? Maybe the sculpture you found at the flea market last summer really is a Remington. How can you find out?"
    :image="{ 
      src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/images/2017-08/Terra%20Seminar%202015_Eleanor.jpg',
      alt: 'some alt text'
    }"  
  >
  </azalea-plant-section-header>
</demo-preview>

<demo-preview title="Full" :class="`mb-8`">
  <azalea-plant-section-header
    variant="full"
    heading-text="How Do I Research My Art?" 
    body-text="Want to learn more about the painting you found while clearing out the attic? What about the drawing that has been hanging in grandma’s hallway since you were a kid? Maybe the sculpture you found at the flea market last summer really is a Remington. How can you find out?"
    :image="{ 
      src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/images/2017-06/research-hero.jpg',
      alt: 'some alt text'
    }"  
  >
  </azalea-plant-section-header>
</demo-preview>

<demo-preview title="Without image" :class="`mb-8`">
    <azalea-plant-section-header
      heading-text="Art + Artists"
      body-text="Discover the nation's first collection of American art and one of the world's largest and most inclusive collections of art made in the United States, is an unparalleled record of the American experience."
    ></azalea-plant-section-header>
</demo-preview>

## Props

- variant
- heading-text
- body-text
- image <span class="text-sm text-tertiary-400">Object</span>
