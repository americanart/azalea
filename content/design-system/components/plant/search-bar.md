---
title: Search Bar
description: A search input form
---

## Examples

<demo-preview title="With default placeholder" :class="`mb-8`">
  <azalea-plant-search-bar></azalea-plant-search-bar>
</demo-preview>

<demo-preview title="With default value" :class="`mb-8`">
  <azalea-plant-search-bar value="Nam June Paik"></azalea-plant-search-bar>
</demo-preview>
