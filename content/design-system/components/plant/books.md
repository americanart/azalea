---
title: Book Content
description: A set of components that show how books should be displayed on various parts of our website
---

### Related Books on an Exhibition, Artist, and Artwork page

<p class="azalea-text-md">On an Exhibition, Artist, or Artwork page, the related books module should appear in the following way</p>

<demo-preview title="Related Books on an Exhibition, Artist, and Artwork page" :class="`mb-8`">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Books
        </h4>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
<section class="mb-4">
  <div class="flex flex-wrap -mx-4">
    <div class="lg:w-1/2 px-4 mb-4 lg:mb-0">
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-10/CGx_cover_100820.jpg?itok=clGTVO5m" alt="">
    </div>
    <div class="lg:w-1/2 px-4">
      <h3>¡Printing the Revolution! The Rise and Impact of Chicano Graphics, 1965 to Now</h3>
      <p class="azalea-text-md">A groundbreaking look at how Chicano graphic artists and their collaborators have used their art to imagine and sustain identities and political viewpoints during the past half century.
      </p>
      <div class="flex mb-6">
        <span class="text-2xl">$49.95</span>
        <div class="flex flex-wrap ml-4">
          <div class="w-full">
               <azalea-sprout-push-button variant="primary" :full-width="true">Buy Now</azalea-sprout-push-button></div>
        </div>
      </div>
      <p class="small">Or write to PubOrd@si.edu Flexicover $49.95; Hardcover, $60</p>
      <div class="pt-4 border-t"><a class="text-indigo-600 hover:underline" href="#"></a></div>
    </div>
  </div>
</section>
    <hr class="border-gray-200 mb-8">
<section class="mb-4">
  <div class="flex flex-wrap -mx-4">
    <div class="lg:w-1/2 px-4 mb-4 lg:mb-0">
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <div class="lg:w-1/2 px-4">
      <h3>Alexander von Humboldt and the United States: Art, Nature, and Culture</h3>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.
      </p>
      <div class="flex mb-6">
        <span class="text-2xl">$75.00</span>
        <div class="flex flex-wrap ml-4">
          <div class="w-full">
               <azalea-sprout-push-button variant="primary" :full-width="true">Buy Now</azalea-sprout-push-button></div>
        </div>
      </div>
      <p class="small">Or write to PubOrd@si.edu, Hardcover, $75</p>
      <div class="pt-4 border-t"><a class="text-indigo-600 hover:underline" href="#"></a></div>
    </div>
  </div>
</section>
</demo-preview>

### Related Books on the books page

<demo-preview title="Related Books on the Books page" :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h3>
          Featured Publications
        </h3>
        <div class="mt-3 flex sm:mt-0 sm:ml-4">
          <a href="#" class="text-indigo-600 hover:underline">See All</a>
        </div>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
</div>
<div class="azalea-grid grid grid-cols-4 gap-2 md:gap-4 lg:gap-6">
  <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
  <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-10/CGx_cover_100820.jpg?itok=clGTVO5m" alt="">
    </div>
    <h4>¡Printing the Revolution! The Rise and Impact of Chicano Graphics, 1965 to Now</h4>
      <p class="azalea-text-md">A groundbreaking look at how Chicano graphic artists and their collaborators have used their art to imagine and sustain identities and political viewpoints during the past half century.</p>
  </div>
  <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
   <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
  <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
  <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
  <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
  <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
    <div class="shadow-sm rounded-md">
    <div>
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <h4>Alexander von Humboldt and the United States: Art, Nature, and Culture</h4>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.</p>
  </div>
</div>
</demo-preview>

### Related Books list on a publication page
