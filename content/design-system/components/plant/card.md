---
title: Card
description: A card is a common design pattern which gives users as a way of taking action to discover more information.
---

Cards commonly exist as links to more detailed Article content, links, and occasionally as content which supplements
a larger subject.

## Examples

<demo-preview title="Event Card" :class="`mb-8`">
  <azalea-plant-card
    title="SAAM Arcade: Game Changers"
    link="#"
    :image="{ src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/images/2018-07/saam_arcade_3.jpeg.jpg', alt: 'this is alt text' }"
    :body="{
      component: 'AzaleaPlantCardBody', 
      props: {
        badge: { 
          variant: 'status',
          status: false,
          size: 'small',
          text: 'Closing Soon'
        },
        startDate: 'July 28, 2021',
        endDate: 'August 30, 2021',
        location: 'Smithsonian American Art Museum',
      }
    }"
  >
  </azalea-plant-card>
</demo-preview>

<demo-preview title="Artwork Cards" :class="`mb-8`">
  <azalea-sprout-flexible-box>
    <azalea-plant-card
      title="Electronic Superhighway: Continental U.S., Alaska, Hawaii"
      link="#"
      :image="{ src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/images/2017-08/electronic_superhighway_hero.jpg', alt: 'this is alt text' }"
      :body="{
        component: 'AzaleaPlantCardBody', 
        props: {
          badge: { 
            variant: 'status',
            status: true,
            size: 'small',
            text: 'On view'
          },
          descriptionList: [
            { term: 'Artist', description: 'Nam June Paik', link: '#' },
            { term: 'Date', description: '1995' }
          ],
          description: 'Nam June Paik is hailed as the father of video art and is credited with the first use of the term in the 1970s. He recognized the potential for people from all parts of the world to collaborate via media, and he knew that media would completely transform our lives.',
        }
      }"
    >
    </azalea-plant-card>
    <azalea-plant-card
      title="Technology"
      link="#"
      :image="{ src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1994/SAAM-1994.29_1.jpg', alt: 'this is alt text' }"
      :body="{
        component: 'AzaleaPlantCardBody', 
        props: {
          badge: { 
            variant: 'status',
            status: false,
            size: 'small',
            text: 'Not on view'
          },
          descriptionList: [
            { term: 'Artist', description: 'Nam June Paik', link: '#' },
            { term: 'Date', description: '1991' }
          ],
          description: 'three-channel video installation with custom-made cabinet; color, silent, continuous loop',
        }
      }"
    >
    </azalea-plant-card>

  </azalea-sprout-flexible-box>
</demo-preview>

<demo-preview title="Content Card with hover effect" :class="`mb-8`">
  <azalea-plant-card
    :as="{ component: 'AzaleaSproutLink', props: { href: '#' } }"
    tag="Talk"
    title="Art Signs Online: An Artful Conversation in ASL"
    :hover-effect="true"
    link="#"
    :image="{ src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/files/images/events/154918382/DgAsCaj5qZ71Ji3o-vTj4wU2.jpg', alt: 'this is alt text' }"
    :body="{
      component: 'AzaleaPlantCardBody', 
      props: {
        startDate: 'July 28, 2021',
        endDate: 'August 30, 2021',
        location: 'Renwick Gallery',
        description: 'Art Signs gallery talks are presented in American Sign Language by Deaf gallery guides. ASL interpreters voice information and observations allowing hearing and Deaf audiences to discover art together under the leadership of a Deaf guide.'
      }
    }"
  >
  </azalea-plant-card>
</demo-preview>

<demo-preview title="Primary with footer action" :class="`mb-8`">
  <azalea-plant-card
    variant="primary"
    title="SAAM Arcade"
    link="#"
  >
    <template #body>
      <p>This is the perfect opportunity to try your hand at making your own video or tablet game. Interested developers, artists, and creators are given one week to create brand-new games.</p>
    </template>
    <template #footer>
      <azalea-sprout-push-button variant="primary-inverse" :full-width="true">Register Now</azalea-sprout-push-button>
    </template>
  </azalea-plant-card>
</demo-preview>

<demo-preview title="Secondary with footer action" :class="`mb-8`">
  <azalea-plant-card
    variant="secondary"
    title="SAAM Arcade"
    link="#"
  >
    <template #body>
      <p>This is the perfect opportunity to try your hand at making your own video or tablet game. Interested developers, artists, and creators are given one week to create brand-new games.</p>
    </template>
    <template #footer>
      <azalea-sprout-push-button variant="secondary-inverse" :full-width="true">Register Now</azalea-sprout-push-button>
    </template>
  </azalea-plant-card>
</demo-preview>

<demo-preview title="Teritary with footer action" :class="`mb-8`">
  <azalea-plant-card
    variant="tertiary"
    title="SAAM Arcade"
    link="#"
  >
    <template #body>
      <p>This is the perfect opportunity to try your hand at making your own video or tablet game. Interested developers, artists, and creators are given one week to create brand-new games.</p>
    </template>
    <template #footer>
      <azalea-sprout-push-button :full-width="true">Register Now</azalea-sprout-push-button>
    </template>
  </azalea-plant-card>
</demo-preview>

## Props

- as <span class="text-sm text-tertiary-400">Object containing properties for the container element or component</span>
- body <span class="text-sm text-tertiary-400">Object containing properties for a "card body" component</span>
- image <span class="text-sm text-tertiary-400">Object containing properties for an image component</span>
- tag
- title
- link
- full-width
- hover-effect
- variant <span class="text-sm text-tertiary-400">primary, secondary, tertiary</span>
