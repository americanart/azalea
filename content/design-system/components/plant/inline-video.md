---
title: Inline Video
description: Video displayed on artwork and artist pages. Comprised of tabs, a video, a transcript, and icons.
---

<demo-preview title="16 x 9 Inline Vimeo Video" :class="`mb-8`">
    <azalea-seedling-content-header heading-text="Related Video" description-text="">
    </azalea-seedling-content-header>
<div class="aspect-w-16 aspect-h-9">
  <iframe src="https://player.vimeo.com/video/146022717?color=0c88dd&title=0&byline=0&portrait=0&badge=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
</div>
  <azalea-seedling-tabs :tabs="[ 
    { name: 'Transcript', current: true, href: '/design-system/components/seedling/tabs#' },
    { name: 'Overview', href: '/design-system/components/seedling/tabs#' },
  ]">
  </azalea-seedling-tabs>
</demo-preview>

<demo-preview title="16 x 9 Inline YouTube Video" :class="`mb-8`">
    <azalea-seedling-content-header heading-text="Related Video" description-text="">
    </azalea-seedling-content-header>
<div class="aspect-w-16 aspect-h-9">
  <iframe src="https://www.youtube.com/embed/QHx8wnhzkhM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
  <azalea-seedling-tabs :tabs="[ 
    { name: 'Transcript', current: true, href: '/design-system/components/seedling/tabs#' },
    { name: 'Overview', href: '/design-system/components/seedling/tabs#' },
  ]">
  </azalea-seedling-tabs>
</demo-preview>
