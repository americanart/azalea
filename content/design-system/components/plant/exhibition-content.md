---
title: Exhibition Content
description: A set of components that show how exhibition content should be displayed on various parts of our website
---

<demo-preview :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Exhibitions
        </h4>
        <div class="mt-3 flex sm:mt-0 sm:ml-4">
          <a href="#">See All</a>
        </div>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
</div>
 <azalea-sprout-stacked-box>
   <div class="w-full rounded-md flex align-top">
       <div class="md:w-2/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1967/SAAM-1967.59.1118_1.jpg?itok=egO2AySv" alt="">
        </div>
        <div class="md:w-2/4 px-8 md:mb-0">
          <h2>Artworks By African Americans From the Collection</h2>
          <h6>August 31, 2016 — February 28, 2017</h6>
            <p class="azalea-text-md">The Smithsonian American Art Museum is home to an extraordinary collection of artworks by African Americans with more than 2,000 objects by more than 200 artists.</p>
            <a href="#">View Exhibition</a>
        </div>
      </div>
    </div>
   <div class="w-full rounded-md flex align-top">
       <div class="md:w-2/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/2012/SAAM-2012.53.1_1.jpg?itok=DL2DndHJ" alt="">
        </div>
        <div class="md:w-2/4 px-8 md:mb-0">
          <h2>¡Printing the Revolution! The Rise and Impact of Chicano Graphics, 1965 to Now</h2>
          <h6>November 20 - November 22, 2020 And May 14, 2021 - August 8, 2021</h6>
            <p class="azalea-text-md">In the 1960s, activist Chicano artists forged a remarkable history of printmaking that remains vital today. Many artists came of age during the civil rights, labor, anti-war, feminist and LGBTQ+ movements and channeled the period’s social activism into assertive aesthetic statements that announced a new political and cultural consciousness among people of Mexican descent in the United States. <em>¡Printing the Revolution!</em> explores the rise of Chicano graphics within these early social movements and the ways in which Chicanx artists since then have advanced innovative printmaking practices attuned to social justice.</p>
            <a href="#">View Exhibition</a>
        </div>
      </div>
    </div>
   <div class="w-full rounded-md flex align-top">
       <div class="md:w-2/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/files/images/2020/SAAM-2020.54.1_2.jpg?itok=cmMmoxTH" alt="">
        </div>
        <div class="md:w-2/4 px-8 md:mb-0">
          <h2>Musical Thinking: New Video Artists in the Smithsonian American Art Museum Collection</h2>
          <h6>June 16, 2023 — January 29, 2024</h6>
            <p class="azalea-text-md">Musical Thinking explores the powerful resonances between recent video art and popular music. The exhibition focuses on video art that employs the strategies of musical creation—scores, improvisation, and interpretation—as well as its styles, structures, and lyrics to speak to personal as well as shared aspects of American life.</p>
            <a href="#">View Exhibition</a>
        </div>
      </div>
    </div>
  </azalea-sprout-stacked-box>
</demo-preview>
