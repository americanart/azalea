---
title: Article Content
description: A set of components that show how article/blog posts should be displayed on various parts of our website
---

<demo-preview :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Articles
        </h4>
        <div class="mt-3 flex sm:mt-0 sm:ml-4">
          <a href="#">See All</a>
        </div>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
</div>
  <azalea-sprout-stacked-box>
    <div class="w-full rounded-md flex flex-wrap align-top">
       <div class="w-full md:w-1/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1980/SAAM-1980.36.9_1.jpg?itok=xsToWtOE" alt="">
        </div>
        <div class="w-full md:w-2/4 md:px-8 md:mb-0">
          <h4>Five Women Changemakers in American Art</h4>
          <h6>September 30th, 2021</h6>
        </div>
        <div class="w-full md:w-1/4 mb-8 md:mb-0">
          <p class="azalea-text-md">SAAM's collection includes several artworks that remind us of the moments of tragedy, the enduring spirit of a nation, and the lasting impact of the events</p>
        </div>
      </div>
    </div>
    <div class="w-full rounded-md flex flex-wrap align-top">
       <div class="w-full md:w-1/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1980/SAAM-1980.36.9_1.jpg?itok=xsToWtOE" alt="">
        </div>
        <div class="w-full md:w-2/4 md:px-8 md:mb-0">
          <h4>Five Women Changemakers in American Art</h4>
          <h6>September 30th, 2021</h6>
        </div>
        <div class="w-full md:w-1/4 mb-8 md:mb-0">
          <p class="azalea-text-md">SAAM's collection includes several artworks that remind us of the moments of tragedy, the enduring spirit of a nation, and the lasting impact of the events</p>
        </div>
      </div>
    </div>
      <div class="w-full rounded-md flex flex-wrap align-top">
       <div class="w-full md:w-1/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1980/SAAM-1980.36.9_1.jpg?itok=xsToWtOE" alt="">
        </div>
        <div class="w-full md:w-2/4 md:px-8 md:mb-0">
          <h4>Five Women Changemakers in American Art</h4>
          <h6>September 30th, 2021</h6>
        </div>
        <div class="w-full md:w-1/4 mb-8 md:mb-0">
          <p class="azalea-text-md">SAAM's collection includes several artworks that remind us of the moments of tragedy, the enduring spirit of a nation, and the lasting impact of the events</p>
        </div>
      </div>
    </div>
  </azalea-sprout-stacked-box>
</demo-preview>
