---
title: Tabular Data
description: A table of data with column and/or row headers.
---

<demo-preview title="Table with column headers" :class="`my-8`">
  <azalea-plant-tabular-data
    caption="Artists"
    :col-headers="['Artist', 'Birth Year', 'Birth City', 'Birth State', 'Nationality']"
    :items="[
      ['Mary Vaux Walcott', '1860', 'Philadelphia', 'Pennsylvania', 'American'],
      ['Alma Thomas', '1891', ' Columbus', 'Georgia', 'American'],
      ['Katherine Schmidt', '1898', 'Xenia', 'Ohio', 'American'],
      ['Berenice Abbott', '1898', 'Springfield', 'Ohio', 'American'],
      ['Judy Youngblood', '1948', 'El Paso', 'Texas', 'American'],
    ]"
  >
  </azalea-plant-tabular-data>
</demo-preview>

<demo-preview title="Table with column headers and row headers" :class="`mb-8`">
  <azalea-plant-tabular-data
    caption="Artists"
    :col-headers="['Artist', 'Birth Year', 'Birth City', 'Birth State', 'Nationality']"
    :items="[
      ['Mary Vaux Walcott', '1860', 'Philadelphia', 'Pennsylvania', 'American'],
      ['Alma Thomas', '1891', ' Columbus', 'Georgia', 'American'],
      ['Katherine Schmidt', '1898', 'Xenia', 'Ohio', 'American'],
      ['Berenice Abbott', '1898', 'Springfield', 'Ohio', 'American'],
      ['Judy Youngblood', '1948', 'El Paso', 'Texas', 'American'],
    ]"
    :row-headers="true"
  >
  </azalea-plant-tabular-data>
</demo-preview>

];
