---
title: Canopy
description: A large decorative banner with media background.
---

## Examples

<demo-preview title="Canopy with an InfoCard" :class="`mb-8`">
  <azalea-plant-canopy image-src="https://s3.amazonaws.com/assets.saam.media/files/files/images/2006/SAAM-2006.1.1_1.jpg">
    <azalea-sprout-flexible-box direction="row" :shrink="true">
      <azalea-plant-card
        variant="primary"
        title="Lure of the West: Treasures from the Smithsonian American Art Museum"
        link="#"
      >
        <template #body>
          <p>In this book, choice paintings and sculptures illustrate changing attitudes toward the West—its landscape, peoples, and development—from the 1820s through the 1940s.</p>
        </template>
        <template #footer>
          <azalea-sprout-push-button variant="primary-inverse" :full-width="true">Buy now</azalea-sprout-push-button>
        </template>
      </azalea-plant-card>
    </azalea-sprout-flexible-box>
  </azalea-plant-canopy>
</demo-preview>

## Props

- image-src


