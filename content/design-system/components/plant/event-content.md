---
title: Event Content
description: Common content list patterns for events
---

<demo-preview :class="`mb-8`">
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Events
        </h4>
        <div class="mt-3 flex sm:mt-0 sm:ml-4">
          <a href="#">See All</a>
        </div>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
</div>
  <azalea-sprout-stacked-box>
    <div class="w-full rounded-md flex flex-wrap align-top">
      <div class="w-full md:w-1/5 md:px-8 md:mb-0">
          <h6>Wednesday</h6>
          <h2>2</h2>
          <h6>DEC</h6>
      </div>
       <div class="w-full md:w-1/5 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1980/SAAM-1980.36.9_1.jpg?itok=xsToWtOE" alt="">
        </div>
        <div class="w-full md:w-2/5 md:px-8 mb-8 md:mb-0">
            <h4>Virtual Handy Hour</h4>
            <h6>Wednesday, December 1, 2021, 7pm EST</h6>
            <p class="azalea-text-md">It’s that time of year, when thoughts turn to relaxing with a warm beverage and your very own hand crafted lavender eye pillow</p>
        </div>
        <div class="w-full md:w-1/5 mb-8 md:mb-0">
            <button class="
                azalea-push-button
                inline-flex
                items-center
                py-2
                px-4
                border border-transparent
                shadow-sm
                hover:shadow-md
                rounded
                no-underline
                whitespace-nowrap
                flex-grow-0
                text-lg
                font-sans font-semibold
                transition-colors 
                duration-1000
                ease-out
                focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-secondary-500
                azalea-touch-action
                primary w-full justify-center">Get Tickets
            </button>
        </div>
      </div>
    </div>
  </azalea-sprout-stacked-box>
</demo-preview>
