---
title: Accordion
description: 
---

Accordion is a collapsible element that is often overused and confusing.

## Examples

<demo-preview :class="`mb-8`">
  <azalea-plant-accordion :items="[
      { active: true, title: 'Is the Smithsonian American Art Museum and Renwick Gallery free?', content: 'Yes. Admission to the Smithsonian American Art Museum and Renwick Gallery is free.'},
      {title: 'Are there storage lockers at the Smithsonian American Art Museum?', content: '<p>There is no coat check or bag storage at the museum and the small lockers in each lobby are not available at this time. </p><p>We encourage you to limit the number of personal belongings and bags you bring into our facilities. Suitcases, large umbrellas, and backpacks are not allowed in the galleries. Security officers may ask you to hand-carry smaller backpacks, or wear them on your front, to protect the artworks. Please note that we do not offer coat check or bag storage at this time. Strollers are permitted in the galleries. </p>'},
      {title: 'Are there accessible bathroom at the Smithsonian American Art Museum?', content: '<p>Accessible restrooms are located on all floors of SAAM.</p>'}
  ]" :collapsible="true"></azalea-plant-accordion>
</demo-preview>

## Accessibility

- aria-expanded
- aria-disabled
- aria-controls
- aria-labelledby

## Props

- items <span class="text-sm text-tertiary-400">Array of content objects with properties `title`, `content`, and `active`</span>
- collapsible <span class="text-sm text-tertiary-400">default: false</span>

