---
title: Drawer
description: An "off-canvas" navigation drawer
---

The drawer is used for quick access to important system level links and actions.

<demo-drawer-preview :class="`my-8`"></demo-drawer-preview>
