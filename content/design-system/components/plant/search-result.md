---
title: Search Result
description: Content overview shown as a search result
---

## Examples

<azalea-plant-search-result>
  <template #media>
    <azalea-sprout-image src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/2016/SAAM-2016.11_6.jpg" alt="The alt text"></azalea-sprout-image>
  </template>
  <template #header>
    <p class="py-2 text-sm text-tertiary-500">https://design.saam.media/design-system/components/plant/search-result</p>
    <azalea-sprout-heading :level="6">Connections: Contemporary Craft at the Renwick Gallery</azalea-sprout-heading>
  </template>
  <template #body>
    <p>Connections is the Renwick Gallery’s dynamic ongoing permanent collection presentation, featuring more than 80 objects celebrating craft as a discipline and an approach to living differently in the modern world.</p>
  </template>
</azalea-plant-search-result>
