---
title: Artwork Toolbar
description: Information and actions for interacting deeply with artwork content
---

<demo-preview title="Artwork Toolbar" :class="`mb-8`">
 <azalea-sprout-icon icon="document-text" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
  <azalea-sprout-icon icon="document-text" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
  <azalea-sprout-icon icon="zoom-in" :class="'mx-auto my-2'" size="medium"></azalea-sprout-icon>
</demo-preview>
