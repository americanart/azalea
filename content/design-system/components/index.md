---
title: Component Library
description: User Interface components for the Azalea Design System. 
---

We've organized components as `sprout`, `seedling`, `plant`, `garden`, because we're clever!


<h2 class="my-4">Installation</h2>

All styles are processed and exported to the `dist` directory. The component library in `components/azalae` can be 
imported and used in any Nuxt.js (or Vue) application. Several components are dependent on other open source packages.

<h3 class="my-4">Dependencies</h3>

- [vue-masonry-wall](https://github.com/fuxingloh/vue-masonry-wall)
- [vue-horizontal](https://github.com/fuxingloh/vue-horizontal)
- [vue-clickaway](https://github.com/simplesmiler/vue-clickaway)
- [vue-plyr](https://github.com/redxtech/vue-plyr#ssr)
- [uuid](https://github.com/uuidjs/uuid)
