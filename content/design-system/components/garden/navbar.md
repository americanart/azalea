---
title: Navbar
description: A navigation element
---

## Examples

<demo-preview :class="`mb-8`">
  <azalea-garden-navbar>
    <template #logo>
      <div class="flex w-full h-full rounded-md bg-primary-900">
        <span class="m-auto text-center text-xs md:text-sm text-white">Logo</span>
      </div>
    </template>
    <template #primary-nav>
      <ul class="flex flex-row justify-end space-x-8 p-0">
        <li><a href="#">Art + Artists</a></li>
        <li><a href="#">Exhibitions + Events</a></li>
        <li><a href="#">About</a></li>
      </ul>
    </template>
    <template #secondary-nav>
      <ul class="flex flex-row justify-end space-x-4 p-0">
        <li><a href="#">Education</a></li>
        <li><a href="#">Research</a></li>
        <li><a href="#">Support</a></li>
      </ul>
    </template>
    <template #top-bar>
      <span>SAAM: open</span>
      <span>Renwick: open</span>
    </template>
    <template #bottom-bar>
      <azalea-seedling-breadcrumb :items="[
          { title: 'Home', link: '/home' },
          { title: 'Art + Artworks', link: '/art/page', active: true},
          { title: 'On View' }
      ]"></azalea-seedling-breadcrumb>
    </template>
  </azalea-garden-navbar>
</demo-preview>

## Slots

- logo
- primary-nav
- secondary-nav
- top-bar
- bottom-bar
- mobile

<h2 class="my-4">Usage</h2>
