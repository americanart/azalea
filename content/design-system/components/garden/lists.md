---
title: Content Lists
description: Common content list patterns
---

Lists display related content.

## Examples

<demo-preview title="Card Grid" :class="`mb-8`">
  <azalea-sprout-container as="section">
    <azalea-seedling-content-header :heading="{as: 'h3', size: 'small', weight: 'semibold' }" heading-text="Events">
      <template #action>
        <azalea-sprout-link href="#">Browse All Upcoming Events</azalea-sprout-link>
      </template>
    </azalea-seedling-content-header>
    <azalea-sprout-grid>
      <azalea-plant-card
        tag="Talk"
        title="Art Signs Online: An Artful Conversation in ASL"
        link="#"
        :image="{ src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/files/images/events/154918382/DgAsCaj5qZ71Ji3o-vTj4wU2.jpg', alt: 'this is alt text' }"
        :body="{ 
          component: 'AzaleaPlantCardBody',
          props: {
          startDate: 'July 28, 2021',
          endDate: 'August 30, 2021',
          location: 'Renwick Gallery'
          }
        }"
        :full-width="true"
      >
        <template #footer>
          <azalea-sprout-push-button :full-width="true" variant="primary">Register Now</azalea-sprout-push-button>
        </template>
      </azalea-plant-card>
      <azalea-plant-card 
        title="Podcast Release: Luce Listening Party with Hometown Sounds and Flowerbomb"
        link="#" 
        :image="{ src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/files/images/events/153321409/DgAPQoUIQq7jgDgu2-f4G5PE.jpg', alt: 'this is alt text' }" 
        :body="{ 
          component: 'AzaleaPlantCardBody',
          props: {
          startDate: 'July 28, 2021',
          endDate: 'August 30, 2021',
          location: 'Smithsonian American Art Museum'
          }
        }"
        :full-width="true"
        >
        <template #footer>
          <azalea-sprout-push-button :full-width="true" variant="primary">Register Now</azalea-sprout-push-button>
        </template>
      </azalea-plant-card>
    </azalea-sprout-grid>
  </azalea-sprout-container>
</demo-preview>
