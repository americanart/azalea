---
title: Sections
description: Common section patterns
---

## Examples

<demo-preview title="Resources Section" :class="`mb-8`">
  <azalea-sprout-container as="section">
    <azalea-seedling-content-header heading-text="K-12 Trainings" description-text="Discover how to integrate American art into your classroom.">
      <template #action>
        <azalea-sprout-link href="#">See all</azalea-sprout-link>
      </template>
    </azalea-seedling-content-header>
    <azalea-sprout-grid>
      <div>
        <azalea-sprout-grid template="2" direction="row">
          <div class="h-44 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
          <div class="h-44 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
          <div class="h-44 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
          <div class="h-44 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
        </azalea-sprout-grid>
      </div>
      <azalea-sprout-image fit="cover" src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/images/2017-10/education_k12_students.jpg" alt="some alt text"></azalea-sprout-image>
    </azalea-sprout-grid>  
</azalea-sprout-container>
</demo-preview>

<demo-preview title="Featured Resource Section" :class="`mb-8`">
  <azalea-sprout-container as="section">
    <azalea-sprout-grid>
      <div>
          <azalea-sprout-image fit="cover" src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/images/2017-10/education_k12_students.jpg" alt="some alt text"></azalea-sprout-image>
    </azalea-sprout-grid>
      </div>
      <div>
      <h3>Recording a Changing Nation</h3>
        <azalea-sprout-body-text as="p" size="small" weight="normal">Use images from the National Endowment for the Art's photographic surveys (or any photographic survey) as source documents to spark sustained inquiry.</azalea-sprout-body-text>
        <azalea-sprout-body-text as="p" size="small" weight="normal">From 1976-1981, the National Endowment for the Arts (NEA) sponsored a program of photographic surveys in 55 communities in 30 states across the United States. These surveys created a new visual record of a changing nation.</azalea-sprout-body-text>
         <azalea-sprout-body-text as="p" size="small" weight="normal" >This resource uses those images (or any photographic survey) as source documents to spark sustained inquiry. Begin with an Analysis activity to provide foundational visual literacy and analysis skills. Then choose to “stack” any of the Lesson Extensions, which uncover images’ complexities, their historical context, or the work of documenting a community. Each activity challenges students to access prior knowledge and apply it in a novel context.</azalea-sprout-body-text>
          <dl>
        <azalea-seedling-description-list-item term="Grade Level">
          <p>6th-12th grade</p>
        </azalea-seedling-description-list-item>
        <azalea-seedling-description-list-item term="Materials">
          <p>Each activity will require access to print or electronic reproductions of select images</p>
        </azalea-seedling-description-list-item>
        <azalea-seedling-description-list-item term="Standards">
          <p>Visual Arts, CCSS Standards, and C3 standards</p>
        </azalea-seedling-description-list-item>
        </dl>
        <azalea-sprout-push-button variant="primary" :full-width="true">View Resource</azalea-sprout-push-button>
      </div>
    </azalea-sprout-grid>

</azalea-sprout-container>
</demo-preview>
