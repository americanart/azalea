---
title: Blog Page
descriptions: Layout and special styles for blog pages
---

  <div class="py-4">
    <hr class="border-t-2 border-black">
    <h1 class="azalea-reader"> This is a title of a blogpost</h1>
    <h2 class="font-light text-primary-700">This is a subtitle of a blogpost</h2>
    <hr class="border-b-1 border-black">
  </div>
  <div class=" bg-white px-6 py-5 flex flex-row space-x-3">
    <div class="flex-shrink-0">
      <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
    </div>
    <div class="flex-shrink">
        <span class="absolute inset-0" aria-hidden="true"></span>
        <p class="text-sm font-medium text-gray-900">
          Pikachu Ninja Turtle
        </p>
        <p class="text-sm text-gray-500 truncate">
          10/11/2021
        </p>
    </div>
     <div class="flex-shrink">
      <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
    </div>
    <div class="flex-1">
        <span class="absolute inset-0" aria-hidden="true"></span>
        <p class="text-sm font-medium text-gray-900">
          Tardis Starcroft
        </p>
        <p class="text-sm text-gray-500 truncate">
          10/11/2021
        </p>
    </div>
  </div>

<div class="azalea-reader">
<div class="pb-24">
 <figure>
    <img class="w-full" src="https://s3.amazonaws.com/dev.saam.media/files/styles/ui_large/s3/images/2021-08/SageK_MariaOakey_202156_RGB_w2000px_cover2.jpg?itok=CYg3ipTA" alt="alt text" width="1124" height="1300">
        <figcaption>Ralph Seymour, Miniature Diorama of John Gellatly Collection, ca. 1924-29, wood, fabric, glass and other materials, 11 7/8 x 12 1/4 x 6 in. (open), Smithsonian American Art Museum, Gift of John Gellatly, 1929.8.530 <figcaption>
</figure>
</div>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<h2>This is an H2 subhead that looks hot</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

  <blockquote>
        <p>Sagittis scelerisque nulla cursus in enim consectetur quam. Dictum urna sed consectetur neque tristique pellentesque. Blandit amet, sed aenean erat arcu morbi.</p>
  </blockquote>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<h3>This is an H3 subhead that looks hot</h3>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<div class="pb-24">
 <figure>
    <img class="w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/files/images/1929/SAAM-1929.8.530_1.jpg?itok=1llw9nfO" alt="alt text" width="1124" height="1300">
        <figcaption>Ralph Seymour, Miniature Diorama of John Gellatly Collection, ca. 1924-29, wood, fabric, glass and other materials, 11 7/8 x 12 1/4 x 6 in. (open), Smithsonian American Art Museum, Gift of John Gellatly, 1929.8.530 <figcaption>
</figure>
</div>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<h4>Heading level 4</h4>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>
<h5>Heading level 5</h5>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Ostendit pedes et pectus. Certe non potest. Certe, nisi voluptatem tanti aestimaretis. Quis hoc dicit? Huius, Lyco, oratione locuples, rebus ipsis ielunior. Haec dicuntur inconstantissime. Pauca mutat vel plura sane.</p>

<h6>Heading level 6</h6>
</div>
