---
title: Slider
description: A horizontal content slider.
---

## Examples

<demo-preview :class="`mb-8`">
  <azalea-garden-slider :items="[
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/images/2017-08/electronic_superhighway_hero.jpg',
        alt: 'Alt text for image 1'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1994/SAAM-1994.29_1.jpg',
        alt: 'Alt text for image 2'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1968/SAAM-1968.155.8_4.jpg',
        alt: 'Alt text for image 3'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1993/SAAM-1993.19_1.jpg',
        alt: 'Alt text for image 4'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1964/SAAM-1964.1.40_1.jpg',
        alt: 'Alt text for image 5'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1986/SAAM-1986.65.138_1.jpg',
        alt: 'Alt text for image 6'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1964/SAAM-1964.1.36_1.jpg',
        alt: 'Alt text for image 7'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1969/SAAM-1969.65.26A_1-000001.jpg',
        alt: 'Alt text for image 8'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1993/SAAM-1993.1.3_3.jpg',
        alt: 'Alt text for image 9'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1970/SAAM-1970.353.1-.116_9.jpg',
        alt: 'Alt text for image 10'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/images/2019-12/Chiura Obara%2C Life and Death%2C Porcupine Flat.jpg',
        alt: 'Alt text for image 11'
      },
    },
    {
      component: 'AzaleaSproutImage',
      props: {
        src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/2011/SAAM-2011.16_1.jpg',
        alt: 'Alt text for image 12'
      },
    }
  ]"></azalea-garden-slider>
</demo-preview>


## Props

- items <span class="text-sm text-tertiary-400">An array of objects used to render dynamic components</span>
