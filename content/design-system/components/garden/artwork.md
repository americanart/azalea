---
title: Artwork Page
description: Common content list patterns
---

<demo-preview title="Artwork" :class="`mb-8`">
  <azalea-plant-section-header
    variant="full"
    heading-text="The Death of Cleopatra" 
    body-text=""
    :image="{ 
      src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1994/SAAM-1994.17_1.jpg?itok=sVqBHcEW',
      alt: 'some alt text'
    }"  
  >
  </azalea-plant-section-header>
<azalea-seedling-tabs :tabs="[ 
    { name: 'Tabs Demo', current: true, href: '/design-system/components/seedling/tabs#' },
    { name: 'Events', href: '/design-system/components/seedling/tabs#' },
    { name: 'Tours', href: '/design-system/components/seedling/tabs#' },
    { name: 'Exhibitions', href: '/design-system/components/seedling/tabs#' },
  ]">
  </azalea-seedling-tabs>
<div class="flex flex-wrap -mx-4 -mb-4 md:mb-0">
  <div class="w-full md:w-1/2 px-4">
    <h2>About the Artwork</h2>
    <div class="azalea-text-md">
    <p>“There is nothing so beautiful as the free forest. To catch a fish when you are hungry, cut the boughs of a tree, make a fire to roast it, and eat it in the open air, is the greatest of all luxuries. I would not stay a week pent up in cities, if it were not for my passion for art.” 
      — Edmonia Lewis, quoted in “Letter From L. Maria Child,” National Anti-Slavery Standard, 27 Feb. 1864.</p>
    <p>Edmonia Lewis, the first professional African-American sculptor, was born in Ohio or New York in 1843 or 1845. Her father was a free African-American and her mother a Chippewa Indian. Orphaned before she was five, Lewis lived with her mother’s nomadic tribe until she was twelve years old. Lewis’s older brother, Sunrise, left the Chippewas and moved to California where he became a gold miner. He financed his sister’s early schooling in Albany, and also helped her to attend Oberlin College in Ohio in 1859. While at Oberlin she shed her Chippewa name ​“Wildfire” and took the name Mary Edmonia Lewis. Her career at Oberlin ended abruptly when she was accused of poisoning two of her white roommates. Lewis was acquitted of the charge, though she had to endure not only a highly publicized trial but also a severe beating by white vigilantes. Subsequently accused of stealing art supplies, she was not permitted to graduate from Oberlin.</p>
    </div>
  </div>

  <div class="lg:w-1/2 md:w-full sm:w-full md:mb-0">
    <div class="sm:rounded-lg">
      <div class="px-4 sm:px-6">
        <h2>
          Artwork Details
        </h2>
        </div>
       <div class="px-4 py-5 sm:p-0">
        <dl>
          <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Title
            </dt>
            <dd class="sm:col-span-2">
              The Death of Cleopatra
            </dd>
          </div>
           <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Artist
            </dt>
            <dd class="sm:col-span-2">
             <a href="/design-system/components/plant/tombstone#">Edmonia Lewis</a>
            </dd>
          </div>
           <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Date
            </dt>
            <dd class="sm:col-span-2">
              carved 1876
            </dd>
          </div>
           <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Location
            </dt>
            <dd class="sm:col-span-2">
              <a href="/design-system/components/plant/tombstone#">Smithsonian American Art Museum</a> / <a href="/design-system/components/plant/tombstone#"> Luce Foundation Center</a> / <a href="/design-system/components/plant/tombstone#"> 3rd Floor </a> / <a href="/design-system/components/plant/tombstone#"> W310 </a>
            </dd>
          </div>
           <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Dimensions
            </dt>
            <dd class="sm:col-span-2">
              63 x 31 1⁄4 x 46 in. (160.0 x 79.4 x 116.8 cm.)            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Credit Line
            </dt>
            <dd class="sm:col-span-2">
              Smithsonian American Art Museum Gift of the Historical Society of Forest Park, Illinois </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Mediums
            </dt>
            <dd class="sm:col-span-2">
              <a href="/design-system/components/plant/tombstone#">marble</a>
            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Mediums Description
            </dt>
            <dd class="sm:col-span-2">
             marble
            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Classifications
            </dt>
            <dd class="sm:col-span-2">
              <a href="/design-system/components/plant/tombstone#">Sculpture</a>
            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Highlights
            </dt>
            <dd class="sm:col-span-2">
              <ul class="font-light">
                <li><a href="/design-system/components/plant/tombstone#">Art by African Americans</a></li>
                <li><a href="/design-system/components/plant/tombstone#">Sculpture</a></li>
              </ul>
            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Keywords
            </dt>
            <dd class="sm:col-span-2">
              <ul class="font-light">
                <li>State of being – death – suicide </li>
                <li>Ethnic – Egyptian</li>
                <li> Animal – reptile – snake</li>
                <li>Figure female – full length</li>
                <li> Portrait female – Cleopatra</li>
                <li> History – ancient – Egypt</li>
              </ul>
            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Object Number
            </dt>
            <dd class="sm:col-span-2">
              1994.17
            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Palette
            </dt>
            <dd class="flex items-center space-x-3  sm:col-span-2"> 
            <div class="w-12 h-12 rounded-full focus:outline-none ring-tertiary-900" style="background-color: #000000">
                            <span class="sr-only">black</span>
                          </div>
            <div class="w-12 h-12 rounded-full focus:outline-none ring-tertiary-900" style="background-color: #696969">
                            <span class="sr-only">dimgrey</span>
                          </div>
            <div class="w-12 h-12 rounded-full focus:outline-none ring-tertiary-900" style="background-color: #a9a9a9">
                            <span class="sr-only">darkgrey</span>
                          </div>
            </dd>
          </div>
         <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 w-full border-tertiary-200 border-t">
            <dt class="border-none text-xl font-medium text-gray-900">
              Attachments
            </dt>
            <dd class="sm:col-span-2">
              <ul role="list" class="border border-gray-200 rounded-md divide-y divide-gray-200">
                <li class="pl-3 pr-4 py-3 flex items-center justify-between text-xl">
                  <div class="w-0 flex-1 flex items-center">
                    <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                      <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd"></path></svg>
                    <span class="ml-2 flex-1 w-0 truncate">
                      artwork-notes.pdf
                    </span>
                  </div>
                  <div class="ml-4 flex-shrink-0">
                    <a href="#" class="font-medium">
                      Download
                    </a>
                  </div>
                </li>
                <li class="pl-3 pr-4 py-3 flex items-center justify-between text-xl">
                  <div class="w-0 flex-1 flex items-center">
                    <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                      <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd"></path></svg>
                    <span class="ml-2 flex-1 w-0 truncate">
                      conservation-report.pdf
                    </span>
                  </div>
                  <div class="ml-4 flex-shrink-0">
                    <a href="#" class="font-medium">
                      Download
                    </a>
                  </div>
                </li>
              </ul>
            </dd>
          </div>
        </dl>
      </div>
    </div>
  </div>
</div>
<div class="container mx-auto">
  <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Articles
        </h4>
        <div class="mt-3 flex sm:mt-0 sm:ml-4">
          <a href="#">See All</a>
        </div>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
</div>
  <azalea-sprout-stacked-box>
    <div class="w-full rounded-md flex align-top">
       <div class="md:w-1/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1980/SAAM-1980.36.9_1.jpg?itok=xsToWtOE" alt="">
        </div>
        <div class="md:w-2/4 px-8 md:mb-0">
          <h4>Five Women Changemakers in American Art</h4>
          <h6>Article Date</h6>
        </div>
        <div class="md:w-1/4 mb-8 md:mb-0">
          <p class="azalea-text-md">SAAM's collection includes several artworks that remind us of the moments of tragedy, the enduring spirit of a nation, and the lasting impact of the events</p>
        </div>
      </div>
    </div>
     <div class="w-full rounded-md flex align-top">
       <div class="md:w-1/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1980/SAAM-1980.36.9_1.jpg?itok=xsToWtOE" alt="">
        </div>
        <div class="md:w-2/4 px-8 md:mb-0">
          <h4>Five Women Changemakers in American Art</h4>
          <h6>Article Date</h6>
        </div>
        <div class="md:w-1/4 mb-8 md:mb-0">
          <p class="azalea-text-md">SAAM's collection includes several artworks that remind us of the moments of tragedy, the enduring spirit of a nation, and the lasting impact of the events</p>
        </div>
      </div>
    </div>
    </div>
    <div class="w-full rounded-md flex align-top">
       <div class="md:w-1/4 px-0 mb-8 md:mb-0">
          <img src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1980/SAAM-1980.36.9_1.jpg?itok=xsToWtOE" alt="">
        </div>
        <div class="md:w-2/4 px-8 md:mb-0">
          <h4>Five Women Changemakers in American Art</h4>
          <h6>Article Date</h6>
        </div>
        <div class="md:w-1/4 mb-8 md:mb-0">
          <p class="azalea-text-md">SAAM's collection includes several artworks that remind us of the moments of tragedy, the enduring spirit of a nation, and the lasting impact of the events</p>
        </div>
      </div>
    </div>
  </azalea-sprout-stacked-box>
    <div>
    <div>
      <hr class="border-t border-black">
      <div class=" sm:flex sm:items-center sm:justify-between mb-8">
        <h4>
          Related Books
        </h4>
      </div>
    </div>
    <hr class="border-gray-200 mb-8">
  </div>
<section class="mb-4">
  <div class="flex flex-wrap -mx-4">
    <div class="lg:w-1/2 px-4 mb-4 lg:mb-0">
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-10/CGx_cover_100820.jpg?itok=clGTVO5m" alt="">
    </div>
    <div class="lg:w-1/2 px-4">
      <h3>¡Printing the Revolution! The Rise and Impact of Chicano Graphics, 1965 to Now</h3>
      <p class="azalea-text-md">A groundbreaking look at how Chicano graphic artists and their collaborators have used their art to imagine and sustain identities and political viewpoints during the past half century.
      </p>
      <div class="flex mb-6">
        <span class="text-2xl">$49.95</span>
        <div class="flex flex-wrap ml-4">
          <div class="w-full">
               <azalea-sprout-push-button variant="primary" :full-width="true">Buy Now</azalea-sprout-push-button></div>
        </div>
      </div>
      <p class="small">Or write to PubOrd@si.edu Flexicover $49.95; Hardcover, $60</p>
      <div class="pt-4 border-t"><a href="#"></a></div>
    </div>
  </div>
</section>
    <hr class="border-gray-200 mb-8">
<section class="mb-4">
  <div class="flex flex-wrap -mx-4">
    <div class="lg:w-1/2 px-4 mb-4 lg:mb-0">
      <img class="rounded shadow w-full" src="https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/images/2020-02/avh_cover1_final.jpg?itok=HdC188xL" alt="">
    </div>
    <div class="lg:w-1/2 px-4">
      <h3>Alexander von Humboldt and the United States: Art, Nature, and Culture</h3>
      <p class="azalea-text-md">Explorer and scientist Alexander von Humboldt left a lasting impression on American visual arts, sciences, literature, and politics.
      </p>
      <div class="flex mb-6">
        <span class="text-2xl">$75.00</span>
        <div class="flex flex-wrap ml-4">
          <div class="w-full">
               <azalea-sprout-push-button variant="primary" :full-width="true">Buy Now</azalea-sprout-push-button></div>
        </div>
      </div>
      <p class="small">Or write to PubOrd@si.edu, Hardcover, $75</p>
      <div class="pt-4 border-t"><a href="#"></a>
    </div>
  </div>
</section>
</demo-preview>
