---
title: Wall
description: A masonry grid layout container.
---

## Examples

<demo-preview :class="`mb-8`">
  <azalea-garden-wall :items="[
    { 
      component: 'AzaleaPlantArtworkCard', 
      props: {
        title: 'Electronic Superhighway: Continental U.S., Alaska, Hawaii',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/images/2017-08/electronic_superhighway_hero.jpg', alt: 'This is alt text' },
        artistName: 'Nam June Paik',
        date: '1995',
        body: 'Nam June Paik is hailed as the father of video art and is credited with the first use of the term in the 1970s. He recognized the potential for people from all parts of the world to collaborate via media, and he knew that media would completely transform our lives',
        onView: true
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'Technology',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1994/SAAM-1994.29_1.jpg', alt: 'This is alt text' },
        artistName: 'Nam June Paik',
        date: '1991',
        body: 'three-channel video installation with custom-made cabinet; color, silent, continuous loop',
        onView: false
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'Model of the Greek Slave',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1968/SAAM-1968.155.8_4.jpg', alt: 'This is alt text' },
        artistName: 'Hiram Powers',
        date: '1843',
        body: 'plaster and metal pins',
        onView: true
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'Death of Rub\én Salazar',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1993/SAAM-1993.19_1.jpg', alt: 'This is alt text' },
        artistName: 'Frank Romero',
        date: '1986',
        body: 'Romero memorializes Rub\én Salazar, a Los Angeles Times journalist and key chronicler of the Chicano civil rights movement. After covering the Chicano Moratorium of 1970, an anti-Vietnam War demonstration, Salazar stopped at the Silver Dollar Café in East LA. Reports of an armed disturbance sent deputies to the scene. A tear-gas projectile shot into the bar killed Salazar instantly. Romero combined references to this tragic day with a vision of the future when Salazar is the subject of a film announced on a theater marquee. The work’s large scale and subject link it with a tradition of grand painting that commemorates events that shaped history.',
        onView: true
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'Valley Farms',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1964/SAAM-1964.1.40_1.jpg', alt: 'This is alt text' },
        artistName: 'Ross Dickinson',
        date: '1934',
        body: 'oil on canvas',
        onView: true
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: '2 Dogs--3 BANDSMEN; and camera',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1986/SAAM-1986.65.138_1.jpg', alt: 'This is alt text' },
        artistName: 'Jon Serl',
        date: '1963',
        body: 'oil on fiberboard',
        onView: true
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'The Farm',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1964/SAAM-1964.1.36_1.jpg', alt: 'This is alt text' },
        artistName: 'Kenjiro Nomura',
        date: '1934',
        body: 'A farm scene with green trees would seem to be a positive view of the American scene, but the painting suggests a hidden threat. Clouds gather and darkness fills the barn and sheds while the foreground road is in shadow. Not a figure or animal is to be seen.',
        onView: true
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'Woman Bathing',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1969/SAAM-1969.65.26A_1-000001.jpg', alt: 'This is alt text' },
        artistName: 'Mary Cassatt',
        date: 'unknown',
        body: 'poster',
        onView: false
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'Yielding to the Ancestors While Controlling the Hands of Time',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1993/SAAM-1993.1.3_3.jpg', alt: 'This is alt text' },
        artistName: 'Lonnie Holley',
        date: 'ca. 1992',
        body: 'oil on wood and metal',
        onView: true
      }
    },
    { 
      component: 'AzaleaPlantArtworkCard',
      props: {
        title: 'The Throne of the Third Heaven of the Nations Millennium General Assembly',
        image: { src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1970/SAAM-1970.353.1-.116_9.jpg', alt: 'This is alt text' },
        artistName: 'James Hampton',
        date: 'ca. 1950-1964',
        onView: true
      }
    }
  ]"></azalea-garden-wall>
</demo-preview>

## Props

- items <span class="text-sm text-tertiary-400">An array of objects used to render dynamic components</span>
