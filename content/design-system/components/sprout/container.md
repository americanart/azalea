---
title: Container
description: A basic container element.
---

## Variants

- as <span class="text-sm text-tertiary-400">The HTML element 'article', 'aside', 'footer', 'header', 'main', or 'section'</span>
- variant <span class="text-sm text-tertiary-400">'primary', 'secondary', or 'tertiary'</span>

## Examples

<demo-preview title="A section" :class="`my-8`">
  <azalea-sprout-container as="section">
    <div class="h-24 w-full p-6 font-medium flex items-center justify-center">A child element</div>
  </azalea-sprout-container>
</demo-preview>

<demo-preview title="A Primary header" :class="`mb-8`">
  <azalea-sprout-container variant="primary" as="header">
    <div class="h-24 w-full p-6 font-medium flex items-center justify-center">A child element</div>
  </azalea-sprout-container>
</demo-preview>

<demo-preview title="A Secondary footer" :class="`mb-8`">
  <azalea-sprout-container variant="secondary" as="footer">
    <div class="h-24 w-full p-6 font-medium flex items-center justify-center">A child element</div>
  </azalea-sprout-container>
</demo-preview>

<demo-preview title="A tertiary container" :class="`mb-8`">
  <azalea-sprout-container variant="tertiary">
    <div class="h-24 w-full p-6 font-medium flex items-center justify-center">A child element</div>
  </azalea-sprout-container>
</demo-preview>

<demo-preview title="Contain max-width" :class="`mb-8`">
  <azalea-sprout-container variant="primary" :contain="true">
    <div class="h-24 w-full p-6 font-medium flex items-center justify-center">A child element</div>
  </azalea-sprout-container>
</demo-preview>
