---
title: Spinner
description: A loading element to inform the use that some action is in progess.
---

## Examples

<demo-preview title="Small" :class="`mb-8`">
  <azalea-sprout-spinner :loading="true" size="small"></azalea-sprout-spinner>
</demo-preview>

<demo-preview title="Medium" :class="`mb-8`">
  <azalea-sprout-spinner :loading="true" size="medium"></azalea-sprout-spinner>
</demo-preview>

<demo-preview title="Large" :class="`mb-8`">
  <azalea-sprout-spinner :loading="true" size="large"></azalea-sprout-spinner>
</demo-preview>

