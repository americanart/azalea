---
title: Body Text
description: A simple text container to ensure consistent body text styles.
---

## Examples

<demo-preview title="Xlarge" :class="`mb-8`">
  <azalea-sprout-body-text as="p" size="xlarge" weight="extrabold">By Jove, my quick study of xlarge extrabold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xlarge" weight="bold">By Jove, my quick study of xlarge bold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xlarge" weight="semibold">By Jove, my quick study of xlarge semibold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xlarge" weight="normal">By Jove, my quick study of xlarge normal lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xlarge" weight="light">By Jove, my quick study of xlarge light lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xlarge" weight="extralight">By Jove, my quick study of xlarge extralight lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xlarge" weight="thin">By Jove, my quick study of xlarge thin lexicography won a prize!</azalea-sprout-body-text>
</demo-preview>

<demo-preview title="Large" :class="`mb-8`">
  <azalea-sprout-body-text as="p" size="large" weight="extrabold">By Jove, my quick study of large extrabold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="large" weight="bold">By Jove, my quick study of large bold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="large" weight="semibold">By Jove, my quick study of large semibold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="large" weight="normal">By Jove, my quick study of large normal lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="large" weight="light">By Jove, my quick study of large light lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="large" weight="extralight">By Jove, my quick study of large extralight lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="large" weight="thin">By Jove, my quick study of large thin lexicography won a prize!</azalea-sprout-body-text>
</demo-preview>

<demo-preview title="Medium" :class="`mb-8`">
  <azalea-sprout-body-text as="p" size="medium" weight="extrabold">By Jove, my quick study of medium extrabold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="bold">By Jove, my quick study of medium bold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="semibold">By Jove, my quick study of medium semibold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="normal">By Jove, my quick study of medium normal lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="light">By Jove, my quick study of medium light lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="extralight">By Jove, my quick study of medium extralight lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="thin">By Jove, my quick study of medium thin lexicography won a prize!</azalea-sprout-body-text>
</demo-preview>

<demo-preview title="Small" :class="`mb-8`">
  <azalea-sprout-body-text as="p" size="small" weight="extrabold">By Jove, my quick study of small extrabold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="small" weight="bold">By Jove, my quick study of small bold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="small" weight="semibold">By Jove, my quick study of small semibold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="small" weight="normal">By Jove, my quick study of small normal lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="small" weight="light">By Jove, my quick study of small light lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="small" weight="extralight">By Jove, my quick study of small extralight lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="small" weight="thin">By Jove, my quick study of small thin lexicography won a prize!</azalea-sprout-body-text>
</demo-preview>

<demo-preview title="Xsmall" :class="`mb-8`">
  <azalea-sprout-body-text as="p" size="xsmall" weight="extrabold">By Jove, my quick study of xsmall extrabold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xsmall" weight="bold">By Jove, my quick study of xsmall bold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xsmall" weight="semibold">By Jove, my quick study of xsmall semibold lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xsmall" weight="normal">By Jove, my quick study of xsmall normal lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xsmall" weight="light">By Jove, my quick study of xsmall light lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xsmall" weight="extralight">By Jove, my quick study of xsmall extralight lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="xsmall" weight="thin">By Jove, my quick study of xsmall thin lexicography won a prize!</azalea-sprout-body-text>
</demo-preview>

<demo-preview title="Alignment" :class="`mb-8`">
  <azalea-sprout-body-text as="p" size="medium" weight="normal">By Jove, my quick study of left aligned lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="normal" align="right">By Jove, my quick study of right aligned lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="normal" align="center">By Jove, my quick study of center aligned lexicography won a prize!</azalea-sprout-body-text>
  <azalea-sprout-body-text as="p" size="medium" weight="normal" align="justify">By Jove, my quick study of justified lexicography won a prize!</azalea-sprout-body-text>
</demo-preview>

<demo-preview title="Prose" :class="`mb-8`">
  <azalea-sprout-body-text as="p" size="medium" weight="normal" :prose="true">By Jove, my quick study of left aligned lexicography won a prize!</azalea-sprout-body-text>
</demo-preview>

## Props

- size <span class="text-sm text-tertiary-400">xsmall, small, medium (default), large, xlarge</span>
- weight <span class="text-sm text-tertiary-400">thin, extralight, light, normal (default), semibold, bold, extrabold</span>
- align <span class="text-sm text-tertiary-400">left (default), right, center, justify</span>
- prose <span class="text-sm text-tertiary-400">true, false (default)</span>
