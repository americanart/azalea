---
title: Figure
description: A graphic element with accomponing semantic description.
---

## Examples

<demo-preview title="Default" :class="`mb-8`">
  <azalea-sprout-figure caption="Nam June Paik, Electronic Superhighway: Continental U.S., Alaska, Hawaii, 1995, fifty-one channel video installation (including one closed-circuit television feed), custom electronics, neon lighting, steel and wood; color, sound, Smithsonian American Art Museum, Gift of the artist, 2002.23, © Nam June Paik Estate ">
    <template #media>
      <azalea-sprout-image 
        src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/images/2017-08/electronic_superhighway_hero.jpg" 
        alt="Atl text"
        title="The image title"
        >
        </azalea-sprout-image>
    </template>
  </azalea-sprout-figure>
</demo-preview>
