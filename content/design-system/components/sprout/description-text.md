---
title: Description Text
description: Common patterns applied to descriptive text elements.
---

- Captions are
- Credits are
- Lead paragraphs are short paragraphs that open an article. Lead paragraphs provide readers a succinct summary of the content and provide a hook to encourage further reading.
- Quotes

## Examples

<demo-preview title="Caption" :class="`mb-8`">
  <azalea-sprout-description-text type="caption">Hiram Powers, Model of the Greek Slave, 1843, plaster and metal pins</azalea-sprout-description-text>
</demo-preview>

<demo-preview title="Credit" :class="`mb-8`">
  <azalea-sprout-description-text type="credit">
    <p>Photo by Libby Weiler.</p>
  </azalea-sprout-description-text>
</demo-preview>

<demo-preview title="Lead" :class="`mb-8`">
  <azalea-sprout-description-text type="lead">Greek Slave became the most famous sculpture of the nineteenth century and propelled the artist, Hiram Powers, to international stardom. The work was so provocatively lifelike that certain exhibition venues in the United States required that men and women view the sculpture separately.</azalea-sprout-description-text>
</demo-preview>

<demo-preview title="Quote" :class="`mb-8`">
  <azalea-sprout-description-text type="quote">This moment is a clarion call. We must commit to working across the lines that divide us to make real the nation so many have long dreamed for, a truly beloved community. As the leader of an institution dedicated to exploring and sharing knowledge for the benefit of humanity, I am more determined than ever to provide our country the resources to help bring the nation together.</azalea-sprout-description-text>
</demo-preview>

## Props

- type <span class="text-sm text-tertiary-400">caption, credit, lead, quote</span>
- align <span class="text-sm text-tertiary-400">left (default), right, center, justify</span>
