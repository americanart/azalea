---
title: Push Button
description: A push button is a button you push.
---

## Variants

- default
- primary
- primary-inverse
- secondary
- secondary-inverse
- success
- warning
- error
- text

## Examples

<demo-preview title="Default" :class="`mb-8`">
 <azalea-sprout-push-button variant="default">Default</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Primary" :class="`mb-8`">
 <azalea-sprout-push-button variant="primary">Primary</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Primary Inverse" :class="`mb-8`">
 <azalea-sprout-push-button variant="primary-inverse">Primary Inverse</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Secondary" :class="`mb-8`">
 <azalea-sprout-push-button variant="secondary">Secondary</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Secondary Inverse" :class="`mb-8`">
 <azalea-sprout-push-button variant="secondary-inverse">Secondary Inverse</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Success" :class="`mb-8`">
 <azalea-sprout-push-button variant="success">Success</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Warning" :class="`mb-8`">
 <azalea-sprout-push-button variant="warning">Warning</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Error" :class="`mb-8`">
 <azalea-sprout-push-button variant="error">Error</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Text" :class="`mb-8`">
 <azalea-sprout-push-button variant="text">Text</azalea-sprout-push-button>
</demo-preview>

<demo-preview title="Block" :class="`mb-8`">
 <azalea-sprout-push-button :full-width="true">Full Width Button</azalea-sprout-push-button>
</demo-preview>

## Props

- variant
- id
- size
- block <span class="text-sm text-tertiary-400">true/false (default)</span>

