---
title: Grid
description: A container element applying common layout patterns using CSS Grid.
---

## Examples

<demo-preview title="Default" :class="`mb-8`">
  <azalea-sprout-grid>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 1</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 2</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
        <p>Utram tandem linguam nescio? Nemo igitur esse beatus potest. Quid sequatur, quid repugnet, vident. Quis hoc dicit? Comprehensum, quod cognitum non habet? Cur post Tarentum ad Archytam? </p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 3</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
        <p>Qui ita affectus, beatum esse numquam probabis; Non quam nostram quidem, inquit Pomponius iocans; Ego vero volo in virtute vim esse quam maximam; Duo Reges: constructio interrete. </p>
        <p>Quis enim redargueret? Nemo igitur esse beatus potest. Laboro autem non sine causa; Quo studio Aristophanem putamus aetatem in litteris duxisse? Itaque contra est, ac dicitis; </p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 4</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
      </div>
    </div>
  </azalea-sprout-grid>
</demo-preview>

<demo-preview title="No gap" :class="`mb-8`">
  <azalea-sprout-grid :gap="false">
    <div class="h-24 p-6 font-medium bg-tertiary-200 flex items-center justify-center">Item 1</div>
    <div class="h-24 p-6 font-medium bg-tertiary-200 flex items-center justify-center">Item 2</div>
    <div class="h-24 p-6 font-medium bg-tertiary-200 flex items-center justify-center">Item 3</div>
    <div class="h-24 p-6 font-medium bg-tertiary-200 flex items-center justify-center">Item 4</div>
    <div class="h-24 p-6 font-medium bg-tertiary-200 flex items-center justify-center">Item 5</div>
    <div class="h-24 p-6 font-medium bg-tertiary-200 flex items-center justify-center">Item 6</div>
  </azalea-sprout-grid>
</demo-preview>

<demo-preview title="Equal sized rows" :class="`mb-8`">
  <azalea-sprout-grid direction="row">
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 1</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 2</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
        <p>Utram tandem linguam nescio? Nemo igitur esse beatus potest. Quid sequatur, quid repugnet, vident. Quis hoc dicit? Comprehensum, quod cognitum non habet? Cur post Tarentum ad Archytam? </p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
        <p>Utram tandem linguam nescio? Nemo igitur esse beatus potest. Quid sequatur, quid repugnet, vident. Quis hoc dicit? Comprehensum, quod cognitum non habet? Cur post Tarentum ad Archytam? </p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 3</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
        <p>Qui ita affectus, beatum esse numquam probabis; Non quam nostram quidem, inquit Pomponius iocans; Ego vero volo in virtute vim esse quam maximam; Duo Reges: constructio interrete. </p>
        <p>Quis enim redargueret? Nemo igitur esse beatus potest. Laboro autem non sine causa; Quo studio Aristophanem putamus aetatem in litteris duxisse? Itaque contra est, ac dicitis; </p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 4</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 5</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
      </div>
    </div>
    <div class="shadow-sm p-6 font-medium bg-white rounded-md">
      <div>
        <p class="font-bold">Item 6</p>
        <p>Si longus, levis. Certe, nisi voluptatem tanti aestimaretis. Itaque his sapiens semper vacabit. Nihil ad rem! Ne sit sane; Si longus, levis. At certe gravius.</p>
      </div>
    </div>
  </azalea-sprout-grid>
</demo-preview>

<demo-preview title="3 column" :class="`mb-8`">
  <azalea-sprout-grid template="3">
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 5</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 6</div>
  </azalea-sprout-grid>
</demo-preview>

<demo-preview title="4 column" :class="`mb-8`">
  <azalea-sprout-grid template="4">
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 5</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 6</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 7</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 8</div>
  </azalea-sprout-grid>
</demo-preview>

<demo-preview title="8 columns (with example col-spans)" :class="`mb-8`">
  <azalea-sprout-grid template="8">
    <div class="col-span-2 shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1 (cols-span-2)</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 5</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 6</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 7</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 8</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 9</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 10</div>
    <div class="col-span-4 shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 11 (col-span-4)</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 12</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 13</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 14</div>
    <div class="col-span-4 shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 15 (col-span-4)</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 16</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 17</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 18</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 19</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 20</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 21</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 21</div>
    <div class=" col-span-3 shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 23 (col-span-3)</div>
    <div class="col-span-8 shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 24 (col-span-8)</div>
  </azalea-sprout-grid>
</demo-preview>



## Props

- direction <span class="text-sm text-tertiary-400">The grid flow: row, col (default)</span>
- template <span class="text-sm text-tertiary-400">Template for number of columns/rows in the grid: 1, 2 (default), 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, none</span>

