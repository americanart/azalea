---
title: Stacked Box
description: A container element for lists with dividers.
---


## Examples

<demo-preview :class="`mb-8`">
  <azalea-sprout-stacked-box>
    <div class="shadow-sm w-full h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm w-full h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm w-full h-64 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
  </azalea-sprout-stacked-box>
</demo-preview>
