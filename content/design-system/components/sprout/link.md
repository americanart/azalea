---
title: Link
description: A common link element.
---

## Examples

<demo-preview :class="`mb-8`">
  <azalea-sprout-link href="/docs" title="Read the documentation">Link to Documentation</azalea-sprout-link>
</demo-preview>

## Props

- href
- title
- target
- external <span class="text-sm text-tertiary-400">default: false</span>

