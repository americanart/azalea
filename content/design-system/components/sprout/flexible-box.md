---
title: Flexible Box
description: A container element applying common layout patterns using CSS Flexbox.
---

## Examples

<demo-preview title="Flex row" :class="`mb-8`">
  <azalea-sprout-flexible-box flex="1" direction="row">
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
  </azalea-sprout-flexible-box>
</demo-preview>

<demo-preview title="Flex row reversed" :class="`mb-8`">
  <azalea-sprout-flexible-box flex="1" :reverse="true">
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
  </azalea-sprout-flexible-box>
</demo-preview>

<demo-preview title="Flex row (No grow/shrink)" :class="`mb-8`">
  <azalea-sprout-flexible-box flex="none" direction="row">
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
  </azalea-sprout-flexible-box>
</demo-preview>

<demo-preview title="Flex column" :class="`mb-8`">
  <azalea-sprout-flexible-box flex="1" direction="col">
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
  </azalea-sprout-flexible-box>
</demo-preview>

<demo-preview title="Flex column reversed" :class="`mb-8`">
  <azalea-sprout-flexible-box flex="1" direction="col" :reverse="true">
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 1</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 2</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 3</div>
    <div class="shadow-sm h-24 p-6 font-medium bg-white rounded-md flex items-center justify-center">Item 4</div>
  </azalea-sprout-flexible-box>
</demo-preview>

## Props

- direction <span class="text-sm text-tertiary-400">row, col (default)</span>
- reverse <span class="text-sm text-tertiary-400">true, false (default)</span>
- wrap <span class="text-sm text-tertiary-400">true (default), false</span>
- grow <span class="text-sm text-tertiary-400">true (default), false</span>
- shrink <span class="text-sm text-tertiary-400">true, false (default)</span>
