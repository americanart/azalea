---
title: Skeleton
description: A loading indicator used as a placeholder for specific content.
---

## Examples

<demo-preview title="Multiple boxes" class="`mb-8`">
  <azalea-sprout-skeleton :num-boxes="5"></azalea-sprout-skeleton>
</demo-preview>

<demo-preview title="Random widths" :class="`mb-8`">
  <azalea-sprout-skeleton :num-boxes="5" :random-width="true"></azalea-sprout-skeleton>
</demo-preview>

<demo-preview title="Full height" :class="`mb-8`">
  <div class="h-64">
    <azalea-sprout-skeleton :full-height="true"></azalea-sprout-skeleton>
  </div>
</demo-preview>

## Props

- num-boxes
- random-width <span class="text-sm text-tertiary-400">default: false</span>
- full-height <span class="text-sm text-tertiary-400">default: false</span>  

