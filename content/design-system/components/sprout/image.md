---
title: Image
description: A basic image.
---

## Examples

<demo-preview title="Default" :class="`mb-8`">
  <div class="w-full h-36 md:h-64 lg:h-96 mx-auto bg-white rounded-md shadow-sm">
    <azalea-sprout-image 
      src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/images/2017-08/electronic_superhighway_hero.jpg" 
      alt="Atl text"
      title="The image title"
      >
      </azalea-sprout-image>
  </div>
</demo-preview>

<demo-preview title="Cover parent container" :class="`mb-8`">
  <div class="w-full h-36 md:h-64 lg:h-96 mx-auto bg-white rounded-md shadow-sm">
    <azalea-sprout-image 
      src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_1300x1300/s3/images/2017-08/electronic_superhighway_hero.jpg" 
      alt="Atl text"
      title="The image title"
      :cover="true"
      >
      </azalea-sprout-image>
  </div>
</demo-preview>

## Props

- alt
- cover <span class="text-sm text-tertiary-400">true, false (default)</span>
- id
- src
- title
