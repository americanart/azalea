---
title: Tag
description: A label or category used to describe content.
---

## Example

<demo-preview :class="`mb-8`">
  <azalea-sprout-tag text="Article"></azalea-sprout-tag>
</demo-preview>

## Props

- text
- size <span class="text-sm text-tertiary-400">xsmall, small, medium (default), large, xlarge</span>

