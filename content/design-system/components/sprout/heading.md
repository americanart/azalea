---
title: Heading
description: A section, article, or page heading.
---

## Examples

<demo-preview title="Xlarge" :class="`mb-8`">
  <azalea-sprout-heading size="xlarge" weight="extrabold">Xlarge extrabold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="bold">Xlarge bold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="semibold">Xlarge semibold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="normal">Xlarge normal heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="light">Xlarge light heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="extralight">Xlarge extralight heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xlarge" weight="thin">Xlarge thin heading text</azalea-sprout-heading>
</demo-preview>

<demo-preview title="Large" :class="`mb-8`">
  <azalea-sprout-heading size="large" weight="extrabold">Large extrabold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="bold">Large bold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="semibold">Large semibold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="normal">Large normal heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="light">Large light heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="extralight">Large extralight heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="large" weight="thin">Large thin heading text</azalea-sprout-heading>
</demo-preview>

<demo-preview title="Medium" :class="`mb-8`">
  <azalea-sprout-heading size="medium" weight="extrabold">Medium extrabold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="bold">Medium bold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="semibold">Medium semibold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="normal">Medium normal heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="light">Medium light heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="extralight">Medium extralight heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="medium" weight="thin">Medium thin heading text</azalea-sprout-heading>
</demo-preview>

<demo-preview title="Small" :class="`mb-8`">
  <azalea-sprout-heading size="small" weight="extrabold">Small extrabold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="bold">Small bold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="semibold">Small semibold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="normal">Small normal heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="light">Small light heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="extralight">Medium extralight heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="small" weight="thin">Small thin heading text</azalea-sprout-heading>
</demo-preview>

<demo-preview title="Xsmall" :class="`mb-8`">
  <azalea-sprout-heading size="xsmall" weight="extrabold">Xsmall extrabold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="bold">Xsmall bold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="semibold">Xsmall semibold heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="normal">Xsmall normal heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="light">Xsmall light heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="extralight">Xsmall extralight heading text</azalea-sprout-heading>
  <azalea-sprout-heading size="xsmall" weight="thin">Xsmall thin heading text</azalea-sprout-heading>
</demo-preview>

## Props

- size <span class="text-sm text-tertiary-400">xsmall, small, medium (default), large, xlarge</span>
- weight <span class="text-sm text-tertiary-400">thin, extralight, light, normal (default), semibold, bold, extrabold</span>
