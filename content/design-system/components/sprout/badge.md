---
title: Badge
description: Badges are used for showing a small amount of information, like a category or number, which deserves attention
---

## Variants

- default
- status

## Examples

<demo-preview title="Default" :class="`mb-8`">
  <azalea-sprout-badge>Default</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Primary" :class="`mb-8`">
  <azalea-sprout-badge variant="primary">Primary</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Secondary" :class="`mb-8`">
  <azalea-sprout-badge variant="secondary">Secondary</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Dismissible" :class="`mb-8`">
  <azalea-sprout-badge :dismissible="true" identifier="hi">Dismissible</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Ghost" :class="`mb-8`">
  <azalea-sprout-badge variant="ghost">Ghost</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Large ghost with image" :class="`mb-8`">
  <azalea-sprout-badge 
    variant="ghost" 
    size="small"
    :image="{ 
      src: 'https://s3.amazonaws.com/assets.saam.media/files/styles/max_325x325/s3/files/images/2015/SAAM-2015.34_1.jpg',
      alt: 'some alt text'
    }"
  >Painting</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Large" :class="`mb-8`">
  <azalea-sprout-badge size="large">Large</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Green Status" :class="`mb-8`">
    <azalea-sprout-badge variant="status" :status="true">Open</azalea-sprout-badge>
</demo-preview>

<demo-preview title="Red Status" :class="`mb-8`">
    <azalea-sprout-badge variant="status" :status="false">Closed</azalea-sprout-badge>
</demo-preview>

## Props

- variant
- status
- size
