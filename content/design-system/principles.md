---
title: Design Principles
---

Principles are the foundation of the Azalea Design System. They form the basis of a good product and help the team with decision making. They are here to guide you and your team when working with the myriad parts of the system and help you do better and more informed decisions.

<h2 class="my-4">Building Gardens</h2>
<span></span><blockquote>"To make a garden is to engage in a planned, demanding long-term enterprise, one peculiarly sensitive to the slings and arrows of fortune." - David Cooper</blockquote>

Gardening is future-oriented. According to Karel Capek, gardeners "live for the future: they want to see, for example, how those birch-trees will be in several years." Gardening requres not only conscious care, but a belief that, "the true, the best is ahead of us... [and] each successive year will add growth and beauty."

<h2 class="my-4">Create compelling experiences</h2>

The Smithsonian lists several [Grand Challenges](https://www.si.edu/about/mission) for the organization, including "Understanding the American Experience", "Magnifying the Transformative Power of Arts and Design", and "Unlocking the Mysteries of the Universe". Wow! These are monumental goals that won't be achieved without taking risks. We must strive to do more than deliver canned content to our audience. Our digital products must **tell stories**, **engage**, **inspire**, and **delight** our users.

<h2 class="my-4">Be consistent</h2>

Consistently applying similar patterns to similar problems is one of the most effective ways to make our users feel comfortable using our products. Similar to how we assist our users finding their way through our museums, we apply consistent design patterns to make our digital products as **intuitive** and **friendly** as possible. We want to be inviting, and therefore our users need to not only feel at home using our digital products, but be able to walk around with the lights off.

<h2 class="my-4">Be inclusive</h2>

Millions of people visit the Smithsonian each year, and we design our digital products with **all** of them in mind, including people with different physical, mental health, social, cultural or learning needs. With each new iteration, we remind ourselves that the word "accessibility", broken down, means the **ability to access**, and evaluate if we've provide that access to everyone.

<h2 class="my-4">Open up</h2>

Create in an **open** and **transparent** manner. Engagement means listening, and being honest about what we are doing, why we are doing it, and it's efficacy. Embrace (and support) [Open Source Software](/docs/open-source.html). Share, collaborate, learn, and iterate together.
