---
title: Viola
description: A headless Content Management System for museums built with Drupal.
order: 1
---

## About

Viola is a Headless Content Management System for the Smithsonian American Art Museum. The project is built on Drupal 
and leverages Drupal's powerful content editing features and JSON:API. It also includes some patterns and utilities 
for ingesting and transforming data, allowing Viola to act as a hub which can serve data from different sources to 
multiple frontend consumers, including sites, kiosks, mobile applications, and digital signage.


## Source

https://gitlab.com/americanart/viola
