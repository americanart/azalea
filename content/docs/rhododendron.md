---
title: Rhododendron
description: A NuxtJS frontend app for the Smithsonian American Art Museum.
order: 2
---

## About

A Nuxt.js frontend application that consumes content from the [Viola Headless CMS](/docs/viola).

## Souce

https://gitlab.com/americanart/rhododendron
