# Azalea

Design System and Documentation site for the [Smithsonian American Art Museum](https://americanart.si.edu)'s digital team.
[Visit the site](https://design.saam.media/).

## Development

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# update the UI styles with your dev changes
$ yarn build:ui
```

## Preview

Preview the static generate site

```bash
$ yarn generate && yarn start
```
