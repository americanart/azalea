#!/usr/bin/env bash
# TODO: Rewrite this as a Node.js script with

# Exit immediately on errors.
set -e
# The Vue component library is copied to the destination.
#COMPONENT_SOURCE="./components/azalea/"
#COMPONENT_DEST="../rhododendron/components/azalea"
# The styles are copied to the destination.
STYLES_SOURCE="./dist/azalea.css"
STYLES_DEST="../saam-drupal-project/web/themes/custom/azalea/dist/css/"
# The icons are copied to the destination.
ICONS_SOURCE="./assets/icons/"
ICONS_DEST="../saam-drupal-project/web/themes/custom/azalea/media/icons"

tput bold
read -r -p "Are you sure you want to copy to ${COMPONENT_DEST}? (this will delete any existing files) [Y/n]" response
tput sgr0
if [[ "$response" =~ ^(yes|Yes|y|Y)$ ]]
then
#    echo -e "Copying components to ${COMPONENT_DEST} ..."
#    rsync -avP --delete ${COMPONENT_SOURCE} ${COMPONENT_DEST}
#    ls -hal ${COMPONENT_DEST}

    echo -e "Copying styles to ${STYLES_DEST} ..."
    rsync -avP ${STYLES_SOURCE} ${STYLES_DEST}
    ls -hal ${STYLES_DEST}

    echo -e "Copying icons to ${ICONS_DEST} ..."
    rsync -avP ${ICONS_SOURCE} ${ICONS_DEST}
    ls -hal ${ICONS_DEST}

else
    echo "Do nothing."
fi

echo -e "Finished"
set +v

